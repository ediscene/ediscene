# EdiScene - Cahier des charges

## Description générale du projet

### Nature

Ediscene a pour but d'offrir un environnement de mise en scène de pièce de théâtre. 

### Objectifs

Fournir à tous les protagonistes d'une pièce de théâtre (Metteur en scène, comédiens) un outil avancé et adapté pour mieux visualiser et se projeter tout au long du processus, des répétitions jusqu'à la représentation. 

## Description de l'environnement de développement envisagé

Ce logiciel sera programmé en **Java** et nous allons utiliser la librairie **Swing** en ce qui concerne l'interface graphique

Pour la persistance des informations, des fichiers au format XML seront 

## Description détaillée des fonctionnalités offertes

### Fonctionnalités obligatoires

- Création/Édition de scène
  - Mise en place
    - Décor
    - Comédiens
  - Déplacement
    - Décor 
    - Comédiens
- Exporter/importer des fichiers de sauvegarde EdiScene
- Visualisation en temps réel du déroulement pièce 

### Fonctionnalités facultatives

## Principe de fonctionnement/mise en oeuvre de l'application



## Mockups


