package views;

import javax.swing.*;
import java.awt.*;

/**
 * Cette classe permet de démarquer nos différents panel
 */
public class BorderPanel extends JPanel {
    public BorderPanel () {
        setBorder(BorderFactory.createLineBorder(Color.black));
    }
}
