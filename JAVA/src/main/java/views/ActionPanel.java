package views;

import Controller.Controller;
import Model.Sauvegarde.Sauvegarde;
import Model.Structure.Acte;
import Model.Structure.Piece;
import Model.Structure.Scene;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.LinkedList;
import java.util.Observable;
import java.util.Observer;

/**
 * Pannel représentant les actions de contrôle sur le model
 * (creation et naviguation dans le model, sauvegarde et chargement)
 */
public class ActionPanel extends BorderPanel {

    private Controller controller;         
    private Piece piece;
    private Sauvegarde _saved;
    //Defini la granularité recherchée
    //  => 0 contrôle sur les actes
    //  => 1 contrôle sur les scènes
    //  => 2 contrôle sur les instants
    private int focus;

    //Représente les indices de position dans la pièce
    private Integer currentActe;
    private Integer currentScene;
    private Integer currentInstant;

    //Label pour le display des indices de position
    private JLabel labelCurrentActe;
    private JLabel labelCurrentScene;
    private JLabel labelCurrentInstant;


    /**
     * Constructeur du panel d'actions
     */
    public ActionPanel(Controller controller) {

        this.piece = controller.getPiece();
        this.controller = controller;
        this._saved = new Sauvegarde(this.piece);
        //Nous travaillons sur des instants
        focus = 2;

        //Définition des boutons
        JButton previous = new JButton("Prev");
        JButton newElement = new JButton("New");
        JButton start = new JButton("Play");
        JButton next = new JButton("Next");
        JButton load = new JButton("Load");
        JButton save = new JButton("Save");

        //Mise à jour des indices de position
        currentActe = controller.getCurrentActe();
        currentScene = controller.getCurrentScene();
        currentInstant = controller.getCurrentInstant();

        int nbScene = piece.getActe(currentActe).getNbScenes();
        int nbInstant = piece.getActe(currentActe).getScene(currentScene).getNbInstants();

        //Définition des labels d'affichages des indices
        labelCurrentActe = new JLabel("Acte: " + (currentActe + 1) + '/' + piece.getNbActes());
        labelCurrentScene = new JLabel("Scene: " + (currentScene + 1) + '/' + nbScene);
        labelCurrentInstant = new JLabel("Instant: " + (currentInstant + 1) + '/' + nbInstant);

        //Définition des boutons de granularité
        JButton acte = new JButton("Acte");
        JButton scene = new JButton("Scene");
        JButton instant = new JButton("Instant");

        //Focus est sur les instants
        acte.setForeground(Color.GRAY);
        scene.setForeground(Color.GRAY);

        //Permet de jouer en fonction de la granularité la pièce, l'acte, la scène
        start.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                switch(focus){
                    case 0 :
                        controller.playPiece();
                        break;
                    case 1:
                        controller.playActe();
                        break;
                    case 2:
                        controller.playScene();
                    default:
                        break;
                }
            }
        });

        //Permet de reculer en fonction de la granularité d'une unité
        previous.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                switch(focus){
                    case 0 :
                        controller.previousActe();
                        break;
                    case 1:
                        controller.previousScene();
                        break;
                    case 2:
                        controller.previousInstant();
                        break;
                    default:
                        break;
                }
                display();
            }
        });

        //Permet d'avancer en fonction de la granularité d'une unité
        next.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                switch(focus){
                    case 0 :
                        controller.nextActe();
                    case 1:
                        controller.nextScene();
                        break;
                    case 2:
                        controller.nextInstant();
                        break;
                    default:
                        break;
                }
                display();
            }
        });

        //Permet de régler la granularité sur acte
        acte.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                focus = 0;
                acte.setForeground(Color.BLACK);
                scene.setForeground(Color.GRAY);
                instant.setForeground(Color.GRAY);
                display();
            }
        });

        //Permet de régler la granularité sur scène
        scene.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                focus = 1;
                scene.setForeground(Color.BLACK);
                acte.setForeground(Color.GRAY);
                instant.setForeground(Color.GRAY);
                display();
            }
        });

        //Permet de régler la granularité sur instant
        instant.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                focus = 2;
                instant.setForeground(Color.BLACK);
                scene.setForeground(Color.GRAY);
                acte.setForeground(Color.GRAY);
                display();
            }
        });


        //Permet d'ajouter en fonction de la granularité un nouvel élément
        newElement.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                switch(focus){
                    case 0 :
                        //controller.createActeAt(currentActe);
                        controller.createActeAppend();
                        break;
                    case 1:
                        //controller.createSceneAt(currentScene);
                        controller.createSceneAppend();
                        break;
                    case 2:
                        //controller.createInstantAt(currentInstant);
                        controller.createInstantAppend();
                        break;
                    default:
                        break;
                }
                display();
            }
        });

        //Permet de sauver la pièce
        save.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try{
                    _saved = new Sauvegarde(piece);
                    // création de la boîte de dialogue
                    JFileChooser dialogue = new JFileChooser();
                    // affichage
                    dialogue.showOpenDialog(null);

                   if(dialogue.getSelectedFile() != null){
                       if( _saved.save(dialogue.getSelectedFile().getAbsolutePath())){
                            System.out.println("Worked");
                       }else{
                           System.out.println("Didn't work");
                       }
                    }
                }
                catch (Exception execpt){
                    execpt.printStackTrace();
                }
            }
        });
        //Permet de charger la piece
        load.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try{
                    _saved = new Sauvegarde(piece);
                    // création de la boîte de dialogue
                    JFileChooser dialogue = new JFileChooser();
                    // affichage
                    dialogue.showOpenDialog(null);
                    if(dialogue.getSelectedFile() != null){
                        if( _saved.load(dialogue.getSelectedFile().getAbsolutePath())){
                            System.out.println("Worked");

                            controller.revertScene();
                            controller.getMainFrame().setVisible(false);
                            controller.getMainFrame().setVisible(true);
                        }else{
                            System.out.println("Didn't work");
                        }
                    }
                }
                catch (Exception execpt){
                    execpt.printStackTrace();
                }
            }
        });
        //Controlle
        add(previous);
        add(newElement);
        add(next);

        add(load);
        add(save);
        add(start);

        //Granularite
        add(instant);
        add(scene);
        add(acte);

        //Display
        add(labelCurrentActe);
        add(labelCurrentScene);
        add(labelCurrentInstant);

    }


    /**
     * Permet de mettre à jour l'affichage
     * des indices de positionnement
     */
    public void display(){

        if(piece != null){

            currentActe = controller.getCurrentActe();
            currentScene = controller.getCurrentScene();
            currentInstant = controller.getCurrentInstant();

            labelCurrentActe.setText("Acte: " + (currentActe + 1) + '/' + piece.getNbActes());

            int nbScene = piece.getActe(currentActe).getNbScenes();
            labelCurrentScene.setText("Scene: " + (currentScene + 1) + '/' + nbScene);

            int nbInstant = piece.getActe(currentActe).getScene(currentScene).getNbInstants();
            labelCurrentInstant.setText("Instant: " + (currentInstant + 1) + '/' + nbInstant);
        }

    }

    /**
     * Retourne la granularité
     * @return la granularité actuelle
     */
    public int getFocus() {
        return focus;
    }
}
