package views;

import Model.Object.ObjectScene;
import Controller.Controller;
import Model.Structure.UsedObjects;
import Model.Timeline.Changes.ChangeOrientation;
import Model.Timeline.Changes.ChangePos;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;


/**
 * Cette classe implémente le panel qui va représenter la scène
 * de théâtre. C'est elle qui affichera les ObjectScene aux positions
 * souhaitées.
 */
public class SceneView extends BorderPanel implements Observer {

    private Controller ctrl = Controller.getInstance();         //Référence au controller
    private UsedObjects usedObjects;                            //ObjectScene actuellement présent
    int x, y;

    private ObjectScene clickedObject;

    public SceneView() {
        setLayout(null);

        addMouseListener(new MouseListener() {

            @Override
            public void mouseClicked(MouseEvent e) {
            }

            /**
             * Permet de récupérer la distance entre un point et un ObjectScene.
             * @param o l'ObjectScene marquant un point
             * @param x la coordonnée x du second point
             * @param y la coordonnée y du second point
             * @return la distance séparant les deux points
             */
            private int sqrDistance(ObjectScene o, int x, int y) {
                return (int) (Math.pow(o.getPosX() - x, 2) + Math.pow(o.getPosY() - y, 2));
            }

            @Override
            public void mousePressed(MouseEvent e) {
                if (usedObjects != null) {

                    ArrayList<ObjectScene> list = usedObjects.getArrayListObjects();// should never be null
                    if (list.size() > 0) {
                        ObjectScene closestObj = list.get(0);
                        int closestDist = sqrDistance(closestObj, e.getX(), e.getY());
                        for (ObjectScene o : usedObjects.getArrayListObjects()) {
                            int dist = sqrDistance(o, e.getX(), e.getY());
                            if (dist < closestDist) {
                                closestObj = o;
                                closestDist = dist;
                            }
                        }
                        clickedObject = closestObj;

                        //Récupère les coordonées pour le ChangePos
                        x = clickedObject.getPosX();
                        y = clickedObject.getPosY();

                        if (e.getButton() == MouseEvent.BUTTON3) {
                            String oldPath = clickedObject.getImage();
                            String newPath = pathRotatedImage(oldPath) + ".png";
                            ctrl.addObjectChange(new ChangeOrientation(clickedObject, newPath));
                            clickedObject.setImage(newPath);
                        }
                    }
                }
            }

            /**
             * Permet de mettre à jour la position de l'objectScene en fin de drag
             * @param e l'événement "relâcher la souris"
             */
            @Override
            public void mouseReleased(MouseEvent e) {
                if (clickedObject != null) {

                    //Ajout du ChangePos
                    clickedObject.setPosX(x);
                    clickedObject.setPosY(y);
                    ctrl.addObjectChange(new ChangePos(clickedObject, e.getX(), e.getY()));

                    clickedObject.setPosX(e.getX());
                    clickedObject.setPosY(e.getY());
                }
            }

            @Override
            public void mouseEntered(MouseEvent e) {
            }

            @Override
            public void mouseExited(MouseEvent e) {
            }
        });

        addMouseMotionListener(new MouseMotionListener() {
            @Override
            public void mouseDragged(MouseEvent e) {
                if (clickedObject != null) {
                    clickedObject.deplacer(e.getX(), e.getY());
                }
            }

            @Override
            public void mouseMoved(MouseEvent e) {
            }
        });
    }

    /**
     * Permet d'afficher un ObjectScene
     * @param o l'ObjectScene à afficher
     * @param g le Graphics sur lequel l'afficher
     */
    private void displayObject(ObjectScene o, Graphics g) {
        if (o.getAffichage()) {
            Image img = Toolkit.getDefaultToolkit().getImage(o.getImage());
            g.drawImage(img, o.getPosX() - o.imageCenterX(), o.getPosY() - o.imageCenterY(), null);
        }
    }


    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        if (usedObjects == null) return;
        for (ObjectScene os : usedObjects.getArrayListObjects()) displayObject(os, g);
    }

    public void setUsedObjects() {
        ctrl.getRightView().repaint();
        this.usedObjects = ctrl.getPiece().getUsedObjects();
        repaint();
    }

    @Override
    public void update(Observable o, Object arg) {
        ctrl.getRightView().repaint();
        usedObjects = ctrl.getPiece().getUsedObjects();
        repaint();
    }

    /**
     * Permet de renomer l'image en fonction de sa rotation
     *
     * @param path le chemin d'accès original à l'image
     * @return le nouveau nom à donner à l'image
     */
    public String pathRotatedImage(String path) {

        path = path.substring(0, path.length() - 4);

        switch (path.charAt(path.length() - 1)) {
            case '1':
                path = path.substring(0, path.length() - 1) + '2';
                break;
            case '2':
                path = path.substring(0, path.length() - 1) + '3';
                break;
            case '3':
                path = path.substring(0, path.length() - 1);
                break;
            default:
                path += '1';
                break;
        }
        return path;
    }

    /**
     * Permet de créer une rotation à 90 degrés
     *
     * @param origin l'image de base sur laquelle travailler
     * @param width  la largeur de l'image
     * @param height la hauteur de l'image
     * @return la nouvelle image après rotation
     */
    public BufferedImage rotate90(BufferedImage origin, int width, int height) {

        BufferedImage result = new BufferedImage(height, width, origin.getType());

        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                result.setRGB(y, width - x - 1, origin.getRGB(x, y));
            }
        }
        return result;
    }

    /**
     * Permet de créer une rotation à 180 degrés
     *
     * @param origin l'image de base sur laquelle travailler
     * @param width  la largeur de l'image
     * @param height la hauteur de l'image
     * @return la nouvelle image après rotation
     */
    public BufferedImage rotate180(BufferedImage origin, int width, int height) {

        BufferedImage result = new BufferedImage(width, height, origin.getType());

        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                result.setRGB(width - x - 1, height - y - 1, origin.getRGB(x, y));
            }
        }
        return result;
    }


    /**
     * Permet de créer une rotation à 270 degrés
     *
     * @param origin l'image de base sur laquelle travailler
     * @param width  la largeur de l'image
     * @param height la hauteur de l'image
     * @return la nouvelle image après rotation
     */
    public BufferedImage rotate270(BufferedImage origin, int width, int height) {

        BufferedImage result = new BufferedImage(height, width, origin.getType());

        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                result.setRGB(height - y - 1, x, origin.getRGB(x, y));
            }
        }
        return result;
    }

    /**
     * Permet de créer les trois autres rotations d'une image
     *
     * @param path le chemin d'accès à l'image
     */
    public void createRotations(String path) {

        File imagefile = new File(path);
        path = path.substring(0, path.length() - 4);        //Retirer l'extension

        try {
            BufferedImage image = ImageIO.read(imagefile);

            int width = image.getWidth();
            int height = image.getHeight();

            if (!(new File(path + "1.png").isFile())) {
                BufferedImage r1 = rotate90(image, width, height);
                ImageIO.write(r1, "png", new File(path + "1.png"));
            }

            if (!(new File(path + "2.png").isFile())) {
                BufferedImage r2 = rotate180(image, width, height);
                ImageIO.write(r2, "png", new File(path + "2.png"));
            }

            if (!(new File(path + "3.png").isFile())) {
                BufferedImage r3 = rotate270(image, width, height);
                ImageIO.write(r3, "png", new File(path + "3.png"));
            }

        } catch (IOException exc) {
            System.out.println("Error while create the image's rotations");
        }
    }
}
