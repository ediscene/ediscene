package views;

import Model.Object.ObjectScene;


/**
 * Cette classe représente le panel permettant de visualiser
 * les propriétés des ObjectScene
 */
public class PropertiesView extends BorderPanel {

    private ObjectScene currentObject;

    /**
     * Constructeur du panel de visualisation des propriétés
     */
    public PropertiesView() {

    }

    /**
     * Permet de rafraîchir les propriétés affichées
     */
    public void displayObjectProperties() {
        removeAll();
        paint(this.getGraphics());          //Corrige le bug de l'affichage de l'humeur pour les mobilier
        if(currentObject != null) add(currentObject.getProperties());
        revalidate();
    }

    /**
     * Permet de modifier l'objet dont les propriétés sont actuellement affichées
     */
    public void setCurrentObject(ObjectScene o) {
        currentObject = o;
    }
}
