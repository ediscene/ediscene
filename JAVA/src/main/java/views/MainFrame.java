package views;

import Controller.Controller;
import Model.Object.Create;
import Model.Structure.Piece;
import Model.Structure.Scene;
import Model.Structure.UsedObjects;

import javax.swing.*;
import java.awt.*;

public class MainFrame extends JFrame {
    Scene scene;
    Create create;
    UsedObjects usedObjects;

    SceneView sceneView;
    RightView rightView;
    ObjectAddView objectAddView;
    RepliqueView repliqueView;
    ActionPanel actionPanel;

    Piece piece;

    public MainFrame() {
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setTitle("EdiScène");
        setLayout(new BorderLayout());
    }

    public void test() {

        Controller controller = Controller.getInstance();

        controller.setMainFrame(this);

        SceneView sceneView = controller.getSceneView();
        RepliqueView repliqueView = controller.getRepliqueView();
        RightView rightView = controller.getRightView();
        ActionPanel actionPanel = controller.getActionPanel();
        ObjectAddView objectAddView = controller.getObjectAddView();

        add(sceneView, BorderLayout.CENTER);
        add(objectAddView, BorderLayout.SOUTH);
        add(repliqueView, BorderLayout.WEST);
        add(actionPanel, BorderLayout.NORTH);
        add(rightView, BorderLayout.EAST);

        repliqueView.displayAllRepliques();
        rightView.getUsedObjectView().displayUsedObjects();

        setSize(900, 600);
        setVisible(true);
    }

    public static void main(String[] args) {
        new MainFrame().test();
    }
}
