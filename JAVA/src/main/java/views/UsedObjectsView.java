package views;

import Controller.Controller;
import Model.Object.ObjectScene;
import Model.Structure.UsedObjects;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collection;


/**
 * Cette classe implémente le panel qui contiendra l'ensemble
 * des objets utilisés dans la pièce.
 */
public class UsedObjectsView extends BorderPanel {
    private UsedObjects usedObjects;

    public UsedObjectsView(UsedObjects usedObjects) {
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        this.usedObjects = usedObjects;
    }

    public void setUsedObjects(UsedObjects usedObjects) {
        this.usedObjects = usedObjects;
    }

    /**
     * Permet d'afficher l'ensemble des objets utilisés dans la
     * pièce dans le panel de droite
     */
    public void displayUsedObjects() {
        removeAll();

        //get Collection of arrayList by classname
        Collection<ArrayList<ObjectScene>> collection = usedObjects.getObjects();
        for(ArrayList<ObjectScene> array : collection) {
            for(ObjectScene o : array) {
                JButton b = new JButton(o.getNom());
                b.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        Controller.getInstance().getRightView().getPropertiesView().setCurrentObject(o);
                        Controller.getInstance().getRightView().getPropertiesView().displayObjectProperties();
                    }
                });
                add(b);
            }
        }

        revalidate();
    }
}
