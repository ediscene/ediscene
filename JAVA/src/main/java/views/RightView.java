package views;

import Model.Structure.UsedObjects;
import javax.swing.*;
import java.awt.*;

/**
 * Cette classe contient l'ensemble des panel de la
 * partie droite de notre interface:
 *      - Le panel contenant l'ensemble des objets utilisés
 *      - Le panel contenant l'ensemble des propriétés d'un ObjectScene
 */
public class RightView extends JPanel {

    private UsedObjectsView usedObjectView;
    private PropertiesView propertiesView;

    public RightView(UsedObjects usedObjects) {
        setLayout(new GridLayout(2, 1));
        usedObjectView = new UsedObjectsView(usedObjects);
        propertiesView = new PropertiesView();
        add(usedObjectView);
        add(propertiesView);
    }

    public void setUsedObjectView(UsedObjects usedObject) {
        this.usedObjectView = new UsedObjectsView(usedObject);
    }

    public UsedObjectsView getUsedObjectView() {
        return usedObjectView;
    }

    public PropertiesView getPropertiesView() {
        return propertiesView;
    }

    public void display() {
        usedObjectView.displayUsedObjects();
        propertiesView.displayObjectProperties();
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        display();
    }
}
