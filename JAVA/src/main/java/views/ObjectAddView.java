package views;

import Controller.Controller;
import Model.Object.*;
import Model.Structure.Scene;

import javax.swing.*;
import java.awt.event.*;

/**
 * Cette classe représente le panel permettant d'ajouter
 * des objectScene dans la pièce
 */
public class ObjectAddView extends BorderPanel {

    Create create;                                      //Fabrique d'ObjectScene
    Controller controller = Controller.getInstance();   //Référence au controller

    /**
     * Constructeur du panel d'ajout d'objets
     */
    public ObjectAddView(Create create) {

        this.create = create;

        //ImageIcon des objectScene pouvant être ajoutés
        ImageIcon addCommedienButton = new ImageIcon("src/main/resources/icônes/perso1.png");
        ImageIcon addMobilierButton = new ImageIcon("src/main/resources/icônes/canapy.png");

        //Boutons pour l'ajout des objectScene
        JButton addComedien = new JButton(addCommedienButton);
        JButton addMobilier = new JButton(addMobilierButton);

        //Creation d'une instance de comédien à l'appui
        addComedien.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                create.createComedien();
            }
        });

        //Creation d'une instance de mobilier à l'appui
        addMobilier.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                create.createMobilier();
            }
        });

        add(addComedien);
        add(addMobilier);

    }
}
