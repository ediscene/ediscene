package views;

import Model.Object.Comedien;
import Model.Object.Replique;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

/**
 * Cette classe représente le panel permettant de visualiser
 * les repliques des comédiens
 */
public class RepliqueView extends BorderPanel {

    private ArrayList<Replique> repliques;
    private JPanel scrollPane;


    public RepliqueView() {
        repliques = new ArrayList<Replique>();
        scrollPane = new JPanel();
        scrollPane.setLayout(new BoxLayout(scrollPane, BoxLayout.Y_AXIS));
        add(scrollPane);
    }

    private class RepliquePanel extends BorderPanel {
        JLabel comedien;
        JTextArea texte;

        public RepliquePanel(Replique r) {
            setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

            comedien = new JLabel(r.getComedien().getNom() + " : ");
            texte = new JTextArea(r.getPhrase());

            comedien.setBorder(BorderFactory.createLineBorder(Color.black));
            comedien.setAlignmentX(Component.LEFT_ALIGNMENT);
            texte.setAlignmentX(Component.LEFT_ALIGNMENT);
            texte.setLineWrap(true);
            texte.setWrapStyleWord(true);

            add(comedien);
            add(texte);
        }
    }

    public void setRepliques(ArrayList<Replique> repliques) {
        this.repliques = repliques;
    }


    public JPanel getRepliquePanel(int i) {
        return new RepliquePanel(repliques.get(i));
    }

    public void displayAllRepliques() {
        scrollPane.removeAll();
        for(int i = 0; i < repliques.size(); i++) {
            scrollPane.add(getRepliquePanel(i));
        }
    }
}
