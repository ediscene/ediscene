package Controller;


import Model.Object.Create;
import Model.Object.ObjectScene;
import Model.Structure.Acte;
import Model.Structure.Piece;
import Model.Structure.Scene;
import Model.Structure.UsedObjects;
import Model.Timeline.Changes.ObjectChange;
import views.*;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;


/**
 * Classe controller gérant les déplacements
 * dans le model (acte, scene, instant) et sa
 * modification
 */
public class Controller {

    //Singleton class
    private static Controller instance = null;

    //Model
    private Piece piece;

    //Références au model
    private Acte acte;
    private Scene scene;
    private UsedObjects usedObjects;

    //Index de position dans le model
    private Integer currentActe = 0;
    private Integer currentScene = 0;
    private Integer currentInstant = 0;

    //Vues
    private MainFrame mainFrame;
    private SceneView sceneView;
    private RepliqueView repliqueView;
    private RightView rightView;
    private ActionPanel actionPanel;
    private ObjectAddView objectAddView;

    //Creation
    private Create create;

    //Private constructor for singleton
    private Controller() {

        instance = this;

        //Main view
        this.sceneView = new SceneView();

        //Model
        this.piece = new Piece(sceneView);
        acte = piece.getActe(currentActe);
        scene = acte.getScene(currentScene);
        usedObjects = piece.getUsedObjects();

        //Creation
        this.create = new Create(scene);

        //Views
        this.repliqueView = new RepliqueView();
        this.rightView = new RightView(usedObjects);
        this.actionPanel = new ActionPanel(this);
        this.objectAddView = new ObjectAddView(create);

    }

    /**
     * Retourne l'instance courante du singleton ou en créé une si il n'y en a pas
     * @return l'instance du controller
     */
    public static Controller getInstance() {
        if (instance == null) {
            instance = new Controller();
        }
        return instance;
    }

    /**
     * Permet de mettre à jour les références à l'acte, la scène et l'instant courant
     */
    public void updateRef(){
        acte = piece.getActe(currentActe);
        scene = acte.getScene(currentScene);
    }

    /**
     * Permet la mise à jour de l'ensemble des vues
     */
    public void display(){
        repliqueView.repaint();
        actionPanel.display();
        rightView.display();
        sceneView.paintComponent(sceneView.getGraphics());
    }

    /**
     * Permet la mise à jour des indices de positionnement dans
     * les labels de actionPanel (destiné uniquement à play)
     */
    public void updateLabelLecture(){
        actionPanel.update(actionPanel.getGraphics());
        actionPanel.paint(actionPanel.getGraphics());
    }

    /**
     * Permet d'ajouter un objectChange à l'instant courant
     * @param objectChange l'objectChange à ajouter
     */
    public void addObjectChange(ObjectChange objectChange){
        scene.getCurrentInstant().add(objectChange);
    }

    //-----------------------Deplacements----------------------------------
    /**
     * passe à l'instant suivant, qu'il soit dans un autre acte, une autre scène, ou dans la même scène
     */
    public void nextInstant(){

        ArrayList<ObjectScene> currentState = usedObjects.getArrayListObjects();

        //Si on est au dernier instant
        if(currentInstant + 1 == scene.getNbInstants()){

            //Si il faut passer à la scène suivante
            if(currentScene + 1 < acte.getNbScenes()){
                currentInstant = 0;
                currentScene++;

            //Si il faut passer à l'acte suivant
            }else if(currentActe + 1 < piece.getNbActes()){
                currentInstant = currentScene = 0;
                currentActe++;
            }

            updateRef();                //Maj références
            create.setScene(scene);     //Maj à quelle scène on doit ajouter des instants

        //Sinon, on passe au prochain instant directe
        }else{
            ++currentInstant;
        }

        //Applique les changements de la scène courante
        for(ObjectScene currentObject : currentState){
            scene.applyChanges(currentObject, currentInstant);
        }
        display();
    }

    /**
     * passe à la scene suivante, qu'elle soit dans un autre acte ou dans le même
     */
    public void nextScene(){

        if(currentInstant == 0)
            nextInstant();
        while (currentInstant != 0)
            nextInstant();

        updateRef();
        create.setScene(scene);

        display();
    }

    /**
     * passe à l'acte suivant
     */
    public void nextActe(){
        if(currentActe + 1 != piece.getNbActes()){

            if(currentInstant == 0)
                nextInstant();
            while(currentInstant != 0 || currentScene != 0){
                nextInstant();
            }

            updateRef();
            create.setScene(scene);
        }

        display();
    }


    /**
     * passe à l'instant précèdent, qu'il soit dans un autre acte, une autre scène, ou dans la même scène
     */
    public void previousInstant(){

        ArrayList<ObjectScene> currentState = usedObjects.getArrayListObjects();

        //Applique les changements de l'instant précédent
        if(currentInstant != 0 || currentScene != 0 || currentActe != 0){
            for(ObjectScene currentObject : currentState){
                scene.removeChanges(currentObject, currentInstant);
            }
            display();
        }

        //Si on est au premier instant
        if(currentInstant == 0){

            if(currentActe != 0 || currentScene != 0){
                //Si il faut passer à la scène précédente
                if(currentScene != 0){
                    --currentScene;

                    //Si il faut passer à l'acte précédent
                }else if(currentActe != 0){
                    -- currentActe;
                    currentScene = piece.getActe(currentActe).getNbScenes() - 1;
                }
                updateRef();
                currentInstant = scene.getNbInstants() - 1;
            }

        }else{
            --currentInstant;
        }

        if(currentInstant == 0) {
            for(ObjectScene currentObject : currentState){
                scene.applyChanges(currentObject, currentInstant);
            }
        }

        display();                  //Maj vues
        updateRef();                //Maj références
        create.setScene(scene);     //Maj à quelle scène on doit ajouter des instants
    }

    /**
     * passe à la scene précèdente, qu'elle soit dans un autre acte ou dans le même
     */
    public void previousScene(){
        if(currentInstant == 0)
            previousInstant();

        while (currentInstant != 0)
            previousInstant();

        updateRef();
        create.setScene(scene);
        display();

    }

    /**
     * passe à l'acte précèdent
     */
    public void previousActe(){
        int tmp = currentActe;

        if(currentActe != 0){
            while(currentInstant != 0 || currentScene != 0 || tmp == currentActe){
                previousInstant();
            }
            updateRef();
            create.setScene(scene);
        }

        display();
    }

    public void revertScene() {
        while(currentInstant != 0){
            previousInstant();
        }
        previousInstant();
        previousInstant();
    }


    //-----------------------Play----------------------------------

    /**
     * Fait défiler les instants de la scène courante
     * à un interval de une seconde pour simuler l'évolution
     * au cours du temps
     */
    public void playScene(){

        ArrayList<ObjectScene> currentState = usedObjects.getArrayListObjects();
        int len = scene.getNbInstants();

        //Remets la scène à l'état initiale
        while(currentInstant != 0){
            previousInstant();
        }

        //Fait défiler les instants
        while(--len != 0){
            updateLabelLecture();
            try{
                TimeUnit.SECONDS.sleep(1);  //Attendre une seconde avant de passer à l'instant suivant
            }catch(InterruptedException e){}
            nextInstant();
        }

        //Mise à jour des vues
        display();
    }

    /**
     * Joue l'acte
     */
    public void playActe(){

        //Remets la scène à l'état initiale
        while(currentInstant != 0 || currentScene != 0){
            previousInstant();
        }

        for(int i = 0; i < acte.getNbScenes(); ++i){
            playScene();
            updateLabelLecture();

            if(i < acte.getNbScenes() - 1){
                try{
                    TimeUnit.SECONDS.sleep(1);
                }catch(InterruptedException e){

                }
                nextInstant();
            }

        }
    }

    /**
     * Joue la pièce
     */
    public void playPiece(){

        //Remets la scène à l'état initiale
        while(currentInstant != 0 || currentScene != 0 || currentActe!= 0){
            previousInstant();
        }

        for(int i = 0; i < piece.getNbActes(); ++i){
            playActe();

            if(i < piece.getNbActes() - 1){
                try{
                    TimeUnit.SECONDS.sleep(1);
                }catch(InterruptedException e){

                }
                nextInstant();
            }
        }

    }

    //-----------------------Create----------------------------------

    /**
     * Créé un acte et l'ajoute à la fin
     */
    public void createActeAppend(){
        Acte newActe = new Acte(sceneView, usedObjects);
        piece.addActe(newActe);
    }

    /**
     * Créé une scène et l'ajoute à la fin
     */
    public void createSceneAppend(){
        Scene scene = new Scene(sceneView);
        acte.addScene(scene);

    }

    /**
     * Créé un instant et l'ajoute à la fin
     */
    public void createInstantAppend(){
        scene.createInstantT();
    }

    /**
     * Créé un acte à un index précis
     * @param index l'index auquel créé l'acte
     */
    public void createActeAt(int index){
        ArrayList<Acte> tmp = new ArrayList<>(piece.getActes());
        tmp.set(index, new Acte(sceneView, usedObjects));

        while(index < tmp.size() - 1){
            tmp.set(++index, piece.getActe(index - 1));
        }
        tmp.add(piece.getActe(piece.getNbActes() - 1));

        piece.setActes(tmp);
    }

    /**
     * Créé une scène à un index précis
     * @param index l'index auquel créé la scène
     */
    public void createSceneAt(int index){
        ArrayList<Scene> tmp = new ArrayList<>(piece.getActe(currentActe).getScenes());
        Acte acte = piece.getActe(currentActe);

        tmp.set(index, new Scene(sceneView));
        while(index < tmp.size() - 1){
            tmp.set(++index, acte.getScene(index - 1));
        }
        tmp.add(acte.getScene(acte.getNbScenes() - 1));

        acte.setScenes(tmp);

    }

    //-----------------------Other----------------------------------

    public Piece getPiece() {
        return piece;
    }

    public void setPiece(Piece piece) {
        this.piece = piece;
    }

    public SceneView getSceneView() {
        return sceneView;
    }

    public void setSceneView(SceneView sceneView) {
        this.sceneView = sceneView;
    }

    public int getCurrentActe() {
        return currentActe;
    }

    public int getCurrentScene() {
        return currentScene;
    }

    public int getCurrentInstant() {
        return currentInstant;
    }

    public RepliqueView getRepliqueView() {
        return repliqueView;
    }

    public RightView getRightView() {
        return rightView;
    }

    public ActionPanel getActionPanel() {
        return actionPanel;
    }

    public ObjectAddView getObjectAddView() {
        return objectAddView;
    }

    public void setCurrentInstant(Integer currentInstant) {
        this.currentInstant = currentInstant;
    }

    public MainFrame getMainFrame() {
        return mainFrame;
    }

    public void setMainFrame(MainFrame mainFrame) {
        this.mainFrame = mainFrame;
    }

    public Acte getActe() {
        return acte;
    }

    public Scene getScene() {
        return scene;
    }
}
