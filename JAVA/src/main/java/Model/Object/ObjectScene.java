package Model.Object;

import Controller.Controller;
import Model.Timeline.Changes.ChangeAffichage;
import Model.Timeline.Changes.ChangeNom;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

/**
 * Cette classe représente les objets à disposer sur la scène
 * (acteurs ou mobilier)
 */
public abstract class ObjectScene {

    private String image;
    private int posX;
    private int posY;
    private String nom;
    private boolean affichage = false;      //Indique si il faut afficher l'objet ou non

    public ObjectScene(String image, int posX, int posY, String nom) {
        this.image = image;
        this.posX = posX;
        this.posY = posY;
        this.nom = nom;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
        Controller.getInstance().display();
    }

    public int getPosX() {
        return posX;
    }

    public void setPosX(int posX) {
        this.posX = posX;
        Controller.getInstance().display();
    }

    public int getPosY() {
        return posY;
    }

    public void setPosY(int posY) {
        this.posY = posY;
        Controller.getInstance().display();
    }

    public void deplacer(int x, int y){
        posX = x;
        posY = y;
        Controller.getInstance().display();
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
        Controller.getInstance().display();
    }

    /**
     * Retourne un panel contenant l'ensemble des propriétés de l'ObjectScene
     * @return le panel contenant l'ensemble des propriétés de l'ObjectScene
     */
    // should be abstract
    public JPanel getProperties() {
        JPanel panel = new JPanel(new GridLayout(0, 2));

        ObjectScene thiRef = this;

        //Changer l'affichage
        JButton displayButton = new JButton("affichage");
        displayButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setAffichage(!getAffichage());
                Controller.getInstance().addObjectChange(new ChangeAffichage(thiRef, getAffichage()));
            }
        });
        panel.add(new JLabel(String.valueOf(getAffichage())));
        panel.add(displayButton);

        //Changer le nom
        panel.add(new JLabel("nom"));
        JTextField nomField = new JTextField(nom);
        nomField.addFocusListener(new FocusListener() {
            public void focusGained(FocusEvent e) {}
            public void focusLost(FocusEvent e) {
                if(!nomField.getText().isEmpty()) {
                    String newNom = nomField.getText();
                    Controller.getInstance().addObjectChange(new ChangeNom(thiRef, newNom));
                    setNom(newNom);
                }
            }
        });
        panel.add(nomField);

        //Changer la position en x
        panel.add(new JLabel("pos x"));
        JTextField posxField = new JTextField("" + posX);
        posxField.addFocusListener(new FocusListener() {
            public void focusGained(FocusEvent e) {}
            public void focusLost(FocusEvent e) {
                try {
                    setPosX(Integer.parseInt(posxField.getText()));
                }catch (Exception ex) {}
            }
        });
        panel.add(posxField);

        //Changer la position en y
        panel.add(new JLabel("pos y"));
        JTextField posyField = new JTextField("" + posY);
        posyField.addFocusListener(new FocusListener() {
            public void focusGained(FocusEvent e) {}
            public void focusLost(FocusEvent e) {
                try {
                    setPosY(Integer.parseInt(posyField.getText()));
                }catch (Exception ex) {}
            }
        });
        panel.add(posyField);

        return panel;
    }

    /**
     * Permet la mise à jour de l'affichage après l'avoir changé
     * @param affichage la nouvelle valeur de affichage
     */
    public void setAffichage(boolean affichage) {
        this.affichage = affichage;
        Controller.getInstance().display();
    }

    public boolean getAffichage() {
        return affichage;
    }

    public int imageCenterX() {
        return 0;
    }

    public int imageCenterY() {
        return 0;
    }

    /**
     * Permet de connaître l'orientation de l'image
     * @return un boolean pour indiqué si l'orientation est verticale ou horizontale
     */
    public boolean getOrientation(){
        char c = (image.charAt(image.length() - 5));
        if(c == '1' || c == '3') return false;
        else return true;
    }
}
