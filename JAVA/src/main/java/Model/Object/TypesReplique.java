package Model.Object;

import java.util.LinkedList;

/**
 * Cette classe représente l'ensemble des différents types de réplique
 */
public class TypesReplique {

    private static LinkedList<String> typesReplique = new LinkedList<>();

    public TypesReplique(){
    }

    public TypesReplique(LinkedList<String> typesReplique) {
        this.typesReplique = typesReplique;
    }

    public TypesReplique(String typeReplique) {
        typesReplique.add(typeReplique);
    }

    public void add(String typeReplique){
        typesReplique.add(typeReplique);
    }

    public void get(int index){
        typesReplique.get(index);
    }

    public int getSize(){
        return typesReplique.size();
    }

    @Override
    public String toString() {

        String str = "Liste des types de replique:\n";

        for(String typeReplique : typesReplique){
            str += typeReplique + '\n';
        }

        return str;
    }

}