package Model.Object;

import Controller.Controller;
import Model.Timeline.Changes.ChangeGenre;
import Model.Timeline.Changes.ChangeHumeur;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

/**
 * Cette classe représente le model physique d'un comédien
 */
public class Comedien extends ObjectScene {

    private boolean genre;
    private String humeur;

    /**
     * Constructeur de comédien
     */
    public Comedien(String image, int posX, int posY, String nom, boolean genre, String humeur) {
        super(image, posX, posY, nom);
        this.genre = genre;
        this.humeur = humeur;
    }

    /**
     * Constructeur de comédien
     */
    public Comedien(String image, int posX, int posY, String nom, boolean genre) {
        this(image, posX, posY, nom, genre, "");
    }

    /**
     * Retourne le genre du comédien
     */
    public boolean isGenre() {
        return genre;
    }

    /**
     * Modifie le genre du comédien
     */
    public void setGenre(boolean genre) {
        this.genre = genre;
        Controller.getInstance().display();
    }

    /**
     * Retourne l'humeur du comédien
     */
    public String getHumeur() {
        return humeur;
    }

    /**
     * Modifie l'humeur du comédien
     */
    public void setHumeur(String humeur) {
        this.humeur = humeur;
        Controller.getInstance().display();
    }

    /**
     * Retourne l'ensemble des propriétés d'un comédien
     * @return un panel contenant l'ensemble des propriétés
     */
    @Override
    public JPanel getProperties() {
        JPanel panel = super.getProperties();

        ObjectScene thiRef = this;


        //Changer le genre du comedien
        panel.add(new JLabel(genre ? "M" : "F"));
        JButton genreButton = new JButton("genre");
        genreButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                boolean newGenre = !isGenre();
                Controller.getInstance().addObjectChange(new ChangeGenre(thiRef, newGenre));
                setGenre(newGenre);
            }
        });
        panel.add(genreButton);

        //Changer l'humeur du comedien
        panel.add(new JLabel("humeur"));
        JTextField humeurField = new JTextField(humeur);
        humeurField.addFocusListener(new FocusListener() {
            public void focusGained(FocusEvent e) {
            }

            public void focusLost(FocusEvent e) {
                if(!humeurField.getText().isEmpty()) {
                    String newHumeur = humeurField.getText();
                    Controller.getInstance().addObjectChange(new ChangeHumeur(thiRef, newHumeur));
                    setHumeur(newHumeur);
                }
            }
        });
        panel.add(humeurField);

        return panel;
    }

    /**
     * Retourne la distance sur l'axe des x entre le centre et le bord
     * de l'image du comédien
     * @return la distance en question
     */
    @Override
    public int imageCenterX() {
        return 50;
    }

    /**
     * Retourne la distance sur l'axe des y entre le centre et le bord
     * de l'image du comédien
     * @return la distance en question
     */
    @Override
    public int imageCenterY() {
        return 50;
    }

}
