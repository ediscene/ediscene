package Model.Object;

import java.util.LinkedList;

/**
 * Cette classe représente la liste des états
 * que peut avoir un mobilier
 */
public class Etats {

    private static LinkedList<String> etats = new LinkedList<>();

    /**
     * Constructeur de Etats
     */
    public Etats(){
    }

    /**
     * Constructeur de Etats
     * @param etats la liste des etats à copier
     */
    public Etats(LinkedList<String> etats) {
        this.etats = etats;
    }

    /**
     * Constructeur de Etats
     * @param etat le nouvel état à ajouter
     */
    public Etats(String etat) {
        etats.add(etat);
    }

    /**
     * Permet d'ajouter un état à la liste
     * @param etat l'état à ajouter
     */
    public void add(String etat){
        etats.add(etat);
    }

    /**
     * Permet de récupérer l'état à l'index désiré
     * @param index l'index de l'état
     */
    public void get(int index){
        etats.get(index);
    }

    /**
     * Permet de récupérer le nombre d'états actuellement stockés
     */
    public int getSize(){
        return etats.size();
    }

    /**
     * Permet de'afficher l'ensemble des états actuellement stockés
     */
    @Override
    public String toString() {

        String str = "Liste des etats:\n";

        for(String etat : etats){
            str += etat + '\n';
        }

        return str;
    }

}