package Model.Object;

import java.util.LinkedList;

/**
 * Cette classe représente les différentes humeurs assignables à
 * un comédien.
 */
public class Humeurs {

    private static LinkedList<String> humeurs = new LinkedList<>();;

    /**
     * Constructeur de Humeurs
     */
    public Humeurs(){
    }

    /**
     * Constructeur de Humeurs
     * @param humeurs la liste des humeurs à copier
     */
    public Humeurs(LinkedList<String> humeurs) {
        this.humeurs = humeurs;
    }

    /**
     * Constructeur de Humeurs
     * @param humeur la nouvelle humeur à ajouter
     */
    public Humeurs(String humeur) {
        humeurs.add(humeur);
    }

    /**
     * Permet d'ajouter une humeur à la liste
     * @param humeur l'humeur à ajouter
     */
    public void add(String humeur){
        humeurs.add(humeur);
    }

    /**
     * Permet de récupérer l'humeur à l'index désiré
     * @param index l'index de l'humeur
     */
    public void get(int index){
        humeurs.get(index);
    }

    /**
     * Permet de récupérer le nombre d'humeurs actuellement stockées
     */
    public int getSize(){
        return humeurs.size();
    }

    /**
     * Permet de'afficher l'ensemble des humeurs actuellement stockées
     */
    @Override
    public String toString() {

        String str = "Liste des humeurs:\n";

        for(String humeur : humeurs){
            str += humeur + '\n';
        }

        return str;
    }

}