package Model.Object;

import Controller.Controller;
import Model.Timeline.Changes.ChangeEtat;

import javax.swing.*;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

/**
 * Cette classe représente le model physique d'un mobilier
 */
public class Mobilier extends ObjectScene {

    private String etat;        //Etat du mobilier

    public Mobilier(String image, int posX, int posY, String nom, String etat) {
        super(image, posX, posY, nom);
        this.etat = etat;
    }

    public Mobilier(String image, int posX, int posY, String nom) {
        this(image, posX, posY, nom, "");
    }

    public String getEtat() {
        return etat;
    }

    public void setEtat(String etat) {
        this.etat = etat;
        Controller.getInstance().display();
    }

    /**
     * Retourne un panel contenant l'ensemble des propriétés du mobilier en question
     * @return le panel contenant l'ensemble des propriétés du mobilier en question
     */
    @Override
    public JPanel getProperties() {
        JPanel panel = super.getProperties();

        ObjectScene thiRef = this;

        panel.add(new JLabel("etat"));
        JTextField etatField = new JTextField(etat);
        etatField.addFocusListener(new FocusListener() {
            public void focusGained(FocusEvent e) {}
            public void focusLost(FocusEvent e) {
                if(!etatField.getText().isEmpty()) {
                    String newEtat = etatField.getText();
                    Controller.getInstance().addObjectChange(new ChangeEtat(thiRef, newEtat));
                    setEtat(newEtat);
                }
            }
        });
        panel.add(etatField);

        return panel;
    }

    /**
     * Retourne la distance sur l'axe des x entre le centre et le bord
     * de l'image du mobilier
     * @return la distance en question
     */
    @Override
    public int imageCenterX() {
        return getOrientation() ? 163 : 60;
    }

    /**
     * Retourne la distance sur l'axe des y entre le centre et le bord
     * de l'image du mobilier
     * @return la distance en question
     */
    @Override
    public int imageCenterY() {
        return getOrientation() ? 60 : 163;
    }
}
