package Model.Object;

import Controller.Controller;
import Model.Structure.Scene;
import Model.Structure.UsedObjects;
import Model.Timeline.Changes.*;

import java.util.Observable;

/**
 * Cette classe représente la fabrique de comédiens et mobiliers.
 * Cette classe est observée par la scène afin de pouvoir lui transmettre
 * une référence au nouvel ObjectScene.
 */
public class Create extends Observable {

    protected Scene scene;                  //Scene à laquelle ajouter le nouvel ObjectScene
    private static int idComedien = 0;      //id du comedien pour une variation des images
    private static int nbComediens = 5;     //nombre d'images de comédien à disposition

    /**
     * Constructeur de Create
     */
    public Create(Scene scene){
        this.scene = scene;
        addObserver(scene);
    }

    /**
     * Retourne la liste des objets utilisés dans la scène courante
     */
    public UsedObjects getUsedObjects() {
        return Controller.getInstance().getPiece().getUsedObjects();
    }

    /**
     * Permet de modifier la scène courante
     */
    public void setScene(Scene scene) {
        this.scene = scene;
    }

    /**
     * Permet de créer une instance de Comedien et ses objectChange correspondants
     * @return l'instance du comédien créé
     */
    public ObjectScene createComedien(){

        if(idComedien == nbComediens)
            idComedien = 0;

        // all default params
        int x = 50;
        int y = 50;
        String nom = "acteur" + (++idComedien);
        boolean genre = true;
        String humeur = "normal";

        Comedien comedien = new Comedien("src/main/resources/imageObjectScene/perso" + idComedien + "_.png", x, y, nom, genre, humeur);

        //Stockage du comédien dans le usedObjects de la scène courante
        Controller.getInstance().getPiece().getUsedObjects().store(comedien);

        setChanged();

        //Ajout des ObjectChange correspondants
        scene.getCurrentInstant().add(new ChangeAffichage(comedien, true));
        scene.getCurrentInstant().add(new ChangePos(comedien, x, y));
        scene.getCurrentInstant().add(new ChangeNom(comedien, nom));
        scene.getCurrentInstant().add(new ChangeGenre(comedien, genre));
        scene.getCurrentInstant().add(new ChangeHumeur(comedien, humeur));

        comedien.setAffichage(true);
        notifyObservers(comedien);

        return comedien;
    }

    /**
     * Permet de créer une instance de Mobilier et ses objectChange correspondants
     * @return l'instance du mobilier créé
     */
    public ObjectScene createMobilier(){

        // all default params
        int x = 163;
        int y = 60;
        String nom = "meuble";
        String etat = "normal";

        Mobilier mobilier = new Mobilier("src/main/resources/imageObjectScene/canapy.png", x, y, nom, etat);

        //Stockage du comédien dans le usedObjects de la scène courante
        Controller.getInstance().getPiece().getUsedObjects().store(mobilier);

        setChanged();

        //Ajout des ObjectChange correspondants
        scene.getCurrentInstant().add(new ChangeAffichage(mobilier, true));
        scene.getCurrentInstant().add(new ChangePos(mobilier, x, y));
        scene.getCurrentInstant().add(new ChangeNom(mobilier, nom));
        scene.getCurrentInstant().add(new ChangeEtat(mobilier, etat));

        mobilier.setAffichage(true);
        notifyObservers(mobilier);


        return mobilier;
    }

}
