package Model.Object;

/**
 * Cette classe représente une replique
 */
public class Replique {

    private String phrase;          //Replique
    private String typeReplique;    //Type de la replique
    private Comedien comedien;      //Comedien associé à la replique

    public Replique(String phrase, Comedien comedien, String typeReplique) {
        this.phrase = phrase;
        this.comedien = comedien;
        this.typeReplique = typeReplique;
    }

    public Replique(String phrase, Comedien comedien) {
        this(phrase, comedien, "");
    }

    public String getPhrase() {
        return phrase;
    }

    public void setPhrase(String phrase) {
        this.phrase = phrase;
    }

    public String getTypeReplqique() {
        return typeReplique;
    }

    public void setTypeReplique(String typeReplique) {
        this.typeReplique = typeReplique;
    }

    public Comedien getComedien() {
        return comedien;
    }

    public void setComedien(Comedien comedien) {
        this.comedien = comedien;
    }
}
