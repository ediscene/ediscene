package Model.Timeline;

import java.util.Iterator;
import java.util.Map;
import java.util.function.Consumer;

/**
 * Iterateur sur les instantT d'une scène, permet de les parcourir dans l'ordre
 * sans se soucier des indices auxquels ils sont attachés
 */
public class TimelineIterator implements Iterator {

    //L'itérateur de map sous-jacent wrappé
    private Iterator<Map.Entry<Integer, InstantT>> insider;

    public TimelineIterator(Map<Integer, InstantT> items) {

        insider = items.entrySet().iterator();
    }

    @Override
    public boolean hasNext() {
        return insider.hasNext();
    }

    @Override
    public InstantT next() {
        return insider.next().getValue();
    }

    @Override
    public void remove() {
        insider.remove();
    }

    @Override
    public void forEachRemaining(Consumer consumer) {

        insider.forEachRemaining(consumer);
    }

}
