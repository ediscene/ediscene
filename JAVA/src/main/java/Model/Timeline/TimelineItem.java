package Model.Timeline;

import Model.Object.ObjectScene;

/**
 * Tous les objets présents dans la timeline doivent implémenter
 * cette interface
 */
public interface TimelineItem {

    /**
     * Retourne l'objet concret lié à cet item
     * @return ObjectScene
     */
    ObjectScene getItem();

    /**
     * Retourne l'objet concret lié à cet item
     * @return ObjectScene
     */
    void change();

    /**
     * Permet de désappliquer l'ObjectChange sur l'ObjectScene associé
     */
    void undo();
}
