package Model.Timeline.Changes;

import Model.Object.ObjectScene;

/**
 * Commande de changement de nom d'un ObjectScene, on lui passe la nouvelle string qui représente
 * le nouveau nom
 */
public class ChangeNom extends ObjectChange{

    String nom;
    String nom_before;

    public String getNom() {
        return nom;
    }

    public String getNom_before() {
        return nom_before;
    }

    public ChangeNom(ObjectScene item, String nom) {
        super(item);
        this.nom = nom;
        this.nom_before = item.getNom();
    }
    public ChangeNom(ObjectScene item, String nom, String nom_before) {
        super(item);
        this.nom = nom;
        this.nom_before = nom_before;
    }

    @Override
    public void change() {
        item.setNom(nom);
    }

    @Override
    public void undo() {
        item.setNom(nom_before);
    }

    @Override
    public void fusionChanges(ObjectChange that) {
        if(that.getClass().equals(this.getClass())) {
            nom_before = ((ChangeNom)that).nom_before;
        }
    }

    @Override
    public void linkChanges(ObjectChange that) {
        if(that.getClass().equals(this.getClass())) {
            nom_before = ((ChangeNom)that).nom;
        }
    }
}
