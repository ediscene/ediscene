package Model.Timeline.Changes;

import Model.Object.Mobilier;
import Model.Object.ObjectScene;

/**
 * Commande de changement de changement d'état d'un mobilier, on lui passe la nouvelle string qui représente
 * le nouvel etat
 */
public class ChangeEtat extends ObjectChange {

    String etat;
    String etat_before;

    public String getEtat() {
        return etat;
    }

    public String getEtat_before() {
        return etat_before;
    }

    public ChangeEtat(ObjectScene item, String etat) {
        super(item);
        this.etat = etat;
        this.etat_before = ((Mobilier)item).getEtat();
    }
    public ChangeEtat(ObjectScene item, String etat, String etat_before) {
        super(item);
        this.etat = etat;
        this.etat_before = etat_before;
    }

    @Override
    public void change() {
        ((Mobilier)item).setEtat(etat);
    }

    @Override
    public void undo() {

        ((Mobilier)item).setEtat(etat_before);
    }

    @Override
    public void fusionChanges(ObjectChange that) {
        if(that.getClass().equals(this.getClass())) {
            etat_before = ((ChangeEtat)that).etat_before;
        }
    }

    @Override
    public void linkChanges(ObjectChange that) {
        if(that.getClass().equals(this.getClass())) {
            etat_before = ((ChangeEtat)that).etat;
        }
    }

}
