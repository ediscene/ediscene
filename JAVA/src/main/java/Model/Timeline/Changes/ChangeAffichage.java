package Model.Timeline.Changes;

import Model.Object.ObjectScene;

/**
 * Commande de changement d'affichage d'un élément ou non, on lui passe le nouveau boolean qui représente
 * le nouvel affichage
 */
public class ChangeAffichage extends ObjectChange{

    boolean affichage;
    boolean affichage_before;

    public boolean isAffichage() {
        return affichage;
    }

    public boolean isAffichage_before() {
        return affichage_before;
    }

    public ChangeAffichage(ObjectScene item, boolean affichage) {
        super(item);
        this.affichage = affichage;
        this.affichage_before = !affichage;
    }

    public ChangeAffichage(ObjectScene item, boolean affichage, boolean affichage_before) {
        super(item);
        this.affichage = affichage;
        this.affichage_before = affichage_before;
    }

    @Override
    public void change() {

        item.setAffichage(affichage);
    }

    @Override
    public void undo() {
        item.setAffichage(affichage_before);
    }

    @Override
    public void fusionChanges(ObjectChange that) {
        if(that.getClass().equals(this.getClass())) {
            affichage_before = ((ChangeAffichage)that).affichage_before;
        }
    }

    @Override
    public void linkChanges(ObjectChange that) {
        if(that.getClass().equals(this.getClass())) {
            affichage_before = ((ChangeAffichage)that).affichage;
        }
    }
}
