package Model.Timeline.Changes;

import Model.Object.ObjectScene;

/**
 * Commande de changement de position, on lui passe les nouveaux entiers qui représentent
 * la nouvelle position
 *
 */
public class ChangePos extends ObjectChange {

    int posX;
    int posY;

    int posX_before;
    int posY_before;

    public int getPosX() {
        return posX;
    }

    public int getPosY() {
        return posY;
    }

    public int getPosX_before() {
        return posX_before;
    }

    public int getPosY_before() {
        return posY_before;
    }

    public ChangePos(ObjectScene item, int posX, int posY){
        super(item);

        this.posX = posX;
        this.posY = posY;

        this.posX_before = item.getPosX();
        this.posY_before = item.getPosY();
    }
    public ChangePos(ObjectScene item, int posX, int posY, int posX_before, int posY_before){
        super(item);

        this.posX = posX;
        this.posY = posY;

        this.posX_before = posX_before;
        this.posY_before = posY_before;
    }
    @Override
    public void change(){
            item.setPosX(posX);
            item.setPosY(posY);
    }

    @Override
    public void undo() {
        item.setPosX(posX_before);
        item.setPosY(posY_before);
    }

    @Override
    public void fusionChanges(ObjectChange that) {
        if(that.getClass().equals(this.getClass())) {
            posX_before = ((ChangePos)that).posX_before;
            posY_before = ((ChangePos)that).posY_before;
        }
    }

    @Override
    public void linkChanges(ObjectChange that) {
        if(that.getClass().equals(this.getClass())) {
            posX_before = ((ChangePos)that).posX;
            posY_before = ((ChangePos)that).posY;
        }
    }
}
