package Model.Timeline.Changes;

import Model.Object.ObjectScene;
import Model.Timeline.TimelineItem;

/**
 * Classe représentant les modifications qui peuvent être
 * associées à un ObjectScene
 */
abstract public class ObjectChange implements TimelineItem {

    ObjectScene item;           //ObjectScene associé

    /**
     * Constructeur de l'ObjectChange
     */
    public ObjectChange(ObjectScene item){
        this.item = item;
    }

    /**
     * Permet de récupérer l'ObjectScene associé
     */
    public ObjectScene getItem(){
        return item;
    }

    /**
     * Permet d'appliquer l'ObjectChange sur l'ObjectScene associé
     */
    public abstract void change();

    /**
     * Permet de désappliquer l'ObjectChange sur l'ObjectScene associé
     */
    public abstract void undo();


    public abstract void fusionChanges(ObjectChange that);

    public abstract void linkChanges(ObjectChange that);
}
