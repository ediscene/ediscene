package Model.Timeline.Changes;

import Model.Object.Comedien;
import Model.Object.Mobilier;
import Model.Object.ObjectScene;

import java.util.List;

/**
 * Commande qui ajoute un ObjectScene de type mobilier à une liste
 */
public class AddMobilier extends ObjectChange {

    private List<ObjectScene> list;

    public String getImage() {
        return item.getImage();
    }

    public int getPosX() {
        return 0;
    }

    public int getPosY() {
        return 0;
    }

    public String getNom() {
        return "";
    }

    public String getEtat() {
        return "";
    }

    public AddMobilier(List<ObjectScene> list, Mobilier mobilier) {
        super(mobilier);
        this.list = list;
    }

    @Override
    public void change() {
        list.add(item);
    }

    @Override
    public void undo() {
        list.remove(item);
    }

    @Override
    public void fusionChanges(ObjectChange that) {}

    @Override
    public void linkChanges(ObjectChange that) {}
}
