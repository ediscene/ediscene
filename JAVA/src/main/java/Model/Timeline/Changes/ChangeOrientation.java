package Model.Timeline.Changes;

import Model.Object.ObjectScene;

/**
 * Commande de changement d'orientation, on lui passe la nouvelle image qui représente
 * la nouvelle orientation
 *
 */
public class ChangeOrientation extends ObjectChange {

    String nouvelle_orientation;
    String ancienne_orientation;

    public String getNouvelle_orientation() {
        return nouvelle_orientation;
    }

    public String getAncienne_orientation() {
        return ancienne_orientation;
    }

    public ChangeOrientation(ObjectScene item, String nouvelle_orientation) {
        super(item);
        this.nouvelle_orientation = nouvelle_orientation;
        this.ancienne_orientation = item.getImage();
    }
    public ChangeOrientation(ObjectScene item, String nouvelle_orientation, String ancienne_orientation) {
        super(item);
        this.nouvelle_orientation = nouvelle_orientation;
        this.ancienne_orientation = ancienne_orientation;
    }


    @Override
    public void change() {
        item.setImage(nouvelle_orientation);
    }

    @Override
    public void undo() {
        item.setImage(ancienne_orientation);
    }

    @Override
    public void fusionChanges(ObjectChange that) {
        if(that.getClass().equals(this.getClass())) {
            ancienne_orientation = ((ChangeOrientation)that).ancienne_orientation;
        }
    }

    @Override
    public void linkChanges(ObjectChange that) {
        if(that.getClass().equals(this.getClass())) {
            ancienne_orientation = ((ChangeOrientation)that).nouvelle_orientation;
        }
    }
}
