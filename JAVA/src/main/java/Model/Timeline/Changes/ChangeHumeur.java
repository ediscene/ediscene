package Model.Timeline.Changes;

import Model.Object.Comedien;
import Model.Object.Mobilier;
import Model.Object.ObjectScene;

/**
 * Commande de changement d'humeur d'un comédien, on lui passe la nouvelle string qui représente
 * la nouvelle humeur
 */
public class ChangeHumeur extends ObjectChange {

    String humeur;
    String humeur_before;

    public String getHumeur() {
        return humeur;
    }

    public String getHumeur_before() {
        return humeur_before;
    }

    public ChangeHumeur(ObjectScene item, String humeur) {
        super(item);
        this.humeur = humeur;
        this.humeur_before = ((Comedien)item).getHumeur();
    }

    public ChangeHumeur(ObjectScene item, String humeur, String humeur_before) {
        super(item);
        this.humeur = humeur;
        this.humeur_before = humeur_before;
    }
    @Override
    public void change() {
        ((Comedien)item).setHumeur(humeur);
    }

    @Override
    public void undo() {
        ((Comedien)item).setHumeur(humeur_before);
    }

    @Override
    public void fusionChanges(ObjectChange that) {
        if(that.getClass().equals(this.getClass())) {
            humeur_before = ((ChangeHumeur)that).humeur_before;
        }
    }

    @Override
    public void linkChanges(ObjectChange that) {
        if(that.getClass().equals(this.getClass())) {
            humeur_before = ((ChangeHumeur)that).humeur;
        }
    }
}
