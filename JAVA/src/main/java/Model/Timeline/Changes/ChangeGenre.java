package Model.Timeline.Changes;

import Model.Object.Comedien;
import Model.Object.ObjectScene;

/**
 * Commande de changement du genre d'un acteur, on lui passe le nouveau boolean qui représente
 * le nouveau genre
 */
public class ChangeGenre extends ObjectChange {

    boolean genre;
    boolean genre_before;

    public boolean isGenre() {
        return genre;
    }

    public boolean isGenre_before() {
        return genre_before;
    }

    public ChangeGenre(ObjectScene item, boolean genre) {
        super(item);
        this.genre = genre;
        this.genre_before = ((Comedien)item).isGenre();
    }
    public ChangeGenre(ObjectScene item, boolean genre, boolean genre_before) {
        super(item);
        this.genre = genre;
        this.genre_before = genre_before;
    }
    @Override
    public void change() {
        ((Comedien)item).setGenre(genre);
    }

    @Override
    public void undo() {
        ((Comedien)item).setGenre(genre_before);
    }

    @Override
    public void fusionChanges(ObjectChange that) {
        if(that.getClass().equals(this.getClass())) {
            genre_before = ((ChangeGenre)that).genre_before;
        }
    }

    @Override
    public void linkChanges(ObjectChange that) {
        if(that.getClass().equals(this.getClass())) {
            genre_before = ((ChangeGenre)that).genre;
        }
    }
}
