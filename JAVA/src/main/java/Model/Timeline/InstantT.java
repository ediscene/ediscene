package Model.Timeline;

import Controller.Controller;
import Model.Object.Replique;
import Model.Timeline.Changes.ObjectChange;

import java.util.ArrayList;

/**
 * Représente un instant sur la timeline. L'unité est la seconde et pour chacune d'elle
 * correspond un InstantT qui peut contenir des ObjectChange, instance de TimelineItem.
 */
public class InstantT {

    //Index de l'instant
    private int index;

    //Les objectChanges qui ont lieu à cet instant
    private ArrayList<TimelineItem> items = new ArrayList<>();

    public InstantT(int i) {
        index = i;
    }

    /**
     * Cette fonction permet de retirer les objectChange
     * se contredisant (deux déplacement au cours du même
     * instant par exemple).
     * @param item l'ObjectChange à comparer
     */
    public void checkDuplicateChange(ObjectChange item) {


        for(TimelineItem c : items) {
            if(c instanceof ObjectChange && c.getClass().equals(item.getClass()) && c.getItem().equals(item.getItem())) {
                item.fusionChanges((ObjectChange)c);
                items.remove(c);
                break;
            }
        }
        //
        Controller ctrl = Controller.getInstance();
        TimelineIterator it = ctrl.getScene().getIteratorFrom(index + 2);
        outerLoop:
        while(it.hasNext()) {
            InstantT that = it.next();
            for(TimelineItem ti : that.items) {
                if(ti instanceof ObjectChange && ti.getClass().equals(item.getClass()) && ti.getItem().equals(item.getItem())) {
                    ((ObjectChange) ti).linkChanges(item);
                    break outerLoop;
                }
            }
        }
    }

    /**
     * Ajoute un ObjectChange à cet Instant
     * @param item l'objectChange
     */
    public void add(TimelineItem item) {
        if(item instanceof ObjectChange) checkDuplicateChange((ObjectChange)item);
        items.add(item);
    }
    /**
     * Ajoute un ObjectChange à cet Instant par la fonction load
     * @param item l'objectChange
     * @param dist distinction enter les deux fonctions
     */
    public void add(TimelineItem item, boolean dist) {
        items.add(item);
    }
    /**
     * Retire un ObjectChange de cet instant
     * @param item à retirer
     */
    public void remove(TimelineItem item) {

        if(items.contains(item))
            items.remove(item);
    }


    /**
     * @return la liste des objectChange
     */
    public ArrayList<TimelineItem> getItems() {
        return items;
    }

    /**
     * @return l'instant sur la timeline
     */
    public int getIndex() {
        return index;
    }

    public void setItems(ArrayList<TimelineItem> items) {
        this.items = items;
    }
}
