package Model.Sauvegarde;

import Controller.Controller;
import Model.Object.Comedien;
import Model.Object.Mobilier;
import Model.Object.ObjectScene;
import Model.Timeline.Changes.*;
import Model.Timeline.TimelineItem;
import Model.Timeline.InstantT;
import Model.Timeline.TimelineIterator;
import com.google.gson.*;
import Model.Structure.*;


import java.io.*;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;


/**
 * Cette classe permet de sauvegarder une pièce.
 */
public class Sauvegarde {

    private Piece _piece;
    private UsedObjects _objects;
    private ArrayList<Acte> _actes;
    private LinkedHashMap<ObjectScene,Integer> _Objetstoid = new LinkedHashMap<>();
    private LinkedHashMap<Integer, ObjectScene> _idtoobjets = new LinkedHashMap<>();


    /**
     * Constructeur de la classe Sauvegarde
     * @param piece la pièce à sauvegarder
     */
    public Sauvegarde(Piece piece){

        _piece = piece;
        _actes = piece.getActes();
        _objects = piece.getUsedObjects();

    }

    /**
     * Sauvegarde la pièce dans un fichier .edi étant serialisé en JSON
     * @param fichier l'addresse du fichier sans le .edi
     * @return true si la sauvegarde a été effectuée, false sinon
     */
    public boolean save(String fichier ){
        try {
            Gson moteurJson = new GsonBuilder().setPrettyPrinting().create();

            BufferedWriter out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fichier + ".edi"), "UTF-8"));

            JsonObject Jpiece = new JsonObject();
            JsonArray Jactes = new JsonArray();

            // Chaque objet à un id unique dans la sauvegarde pour pouvoir les recréer dans les bonnes conditions.
            int id =0;
            JsonArray JObjets = new JsonArray();


            //Création des objets Json du mobilier
            List<ObjectScene> mobiliers = _objects.getAllOfClass(Mobilier.class.getName());
            if(mobiliers != null) {
                for (ObjectScene obj : mobiliers) {
                    JsonObject Jobjet = new JsonObject();
                    Jobjet.add("id", new JsonPrimitive(++id));
                    Jobjet.add("type", new JsonPrimitive("Mobilier"));
                    Jobjet.add("nom", new JsonPrimitive(obj.getNom()));
                    Jobjet.add("posX", new JsonPrimitive(obj.getPosX()));
                    Jobjet.add("posY", new JsonPrimitive(obj.getPosY()));
                    Jobjet.add("image", new JsonPrimitive(obj.getImage()));
                    Jobjet.add("etat", new JsonPrimitive(((Mobilier) obj).getEtat()));

                    JObjets.add(Jobjet);

                    _Objetstoid.put(obj, id);

                }
            }

            //Création des objets Json des comédiens
            List<ObjectScene> comediens = _objects.getAllOfClass(Comedien.class.getName());
            if(comediens != null) {
                for (ObjectScene com : comediens) {
                    JsonObject Jobjet = new JsonObject();
                    Jobjet.add("id", new JsonPrimitive(++id));
                    Jobjet.add("type", new JsonPrimitive("Comedien"));
                    Jobjet.add("nom", new JsonPrimitive(com.getNom()));
                    Jobjet.add("posX", new JsonPrimitive(com.getPosX()));
                    Jobjet.add("posY", new JsonPrimitive(com.getPosY()));
                    Jobjet.add("image", new JsonPrimitive(com.getImage()));
                    Jobjet.add("humeur", new JsonPrimitive(((Comedien) com).getHumeur()));
                    Jobjet.add("genre", new JsonPrimitive(((Comedien) com).isGenre()));

                    JObjets.add(Jobjet);

                    _Objetstoid.put(com, id);

                }
            }

            Jpiece.add("ListObjet", JObjets);

            for (Acte acte : _actes) {
                JsonObject Jacte = new JsonObject();
                JsonArray Jscenes = new JsonArray();
                for (Scene scene : acte.getScenes()) {
                    JsonObject Jscene = new JsonObject();


                    // Gestion des modification au fils du temps de la pièce.
                    TimelineIterator it = scene.getIterator();
                    JsonArray Jinstants = new JsonArray();
                    while (it.hasNext()){
                        InstantT t = it.next();
                        JsonObject Jinstant = new JsonObject();
                        JsonArray Jobjectschanges = new JsonArray();
                        for(TimelineItem change : t.getItems()){
                            JsonObject JobjetChange = new JsonObject();

                            JobjetChange.add("ID_Item", new JsonPrimitive(_Objetstoid.get(change.getItem())));
                            JobjetChange.add("Info",getInfo(change));


                            Jobjectschanges.add(JobjetChange);
                        }
                        Jinstant.add("ID", new JsonPrimitive(t.getIndex()));
                        Jinstant.add("Changes",Jobjectschanges);
                        Jinstants.add(Jinstant);
                    }
                    Jscene.add("instants", Jinstants);


                    Jscenes.add(Jscene);
                }
                Jacte.add("scenes", Jscenes);

                Jactes.add(Jacte);
            }
            Jpiece.add("actes", Jactes);
            out.write(moteurJson.toJson(Jpiece));
            out.close();
        }
        catch (Exception e){
            e.printStackTrace();
            return false;
        }

        return true;
    }

    /**
     * Classe interne servant à la representation des données JSON
     * Cette classe représente tout le fichier
     */
    private class fetchJson{
        JActe [] actes;
        JObjet [] ListObjet;

        public JObjet[] getListObjet() {
            return ListObjet;
        }

        public void setListObjet(JObjet[] listObjet) {
            ListObjet = listObjet;
        }

        public JActe[] getactes() {
            return actes;
        }

        public void setactes(JActe[] actes) {
            this.actes = actes;
        }
    }

    /**
     * Classe interne servant à la representation des données JSON
     * Cette classe représente les actes contenu dans le JSON
     */
    private class JActe{
        JScene [] scenes;

        public JScene[] getscenes() {
            return scenes;
        }

        public void setscenes(JScene[] scenes) {
            this.scenes = scenes;
        }
    }

    /**
     * Classe interne servant à la representation des données JSON
     * Cette classe représente les scènes contenu dans le JSON
     */
    private class JScene{
        Jinstant [] instants;


        public Jinstant[] getInstants() {
            return instants;
        }

        public void setInstants(Jinstant[] instants) {
            this.instants = instants;
        }
    }
    /**
     * Classe interne servant à la representation des données JSON
     * Cette classe représente les objets de la scène contenu dans le JSON
     */
    private class JObjet{
        Integer id;
        String nom;
        String type;
        String image;
        String etat;
        String humeur;
        boolean genre;
        int posX, posY;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getnom() {
            return nom;
        }

        public void setnom(String nom) {
            nom = nom;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getEtat() {
            return etat;
        }

        public void setEtat(String etat) {
            this.etat = etat;
        }

        public String getHumeur() {
            return humeur;
        }

        public void setHumeur(String humeur) {
            this.humeur = humeur;
        }

        public boolean isGenre() {
            return genre;
        }

        public void setGenre(boolean genre) {
            this.genre = genre;
        }

        public int getPosX() {
            return posX;
        }

        public void setPosX(int posX) {
            this.posX = posX;
        }

        public int getPosY() {
            return posY;
        }

        public void setPosY(int posY) {
            this.posY = posY;
        }
    }

    /**
     * Classe interne servant à la representation des données JSON
     * Cette classe représente les instants contenu dans le JSON
     */
    private class Jinstant{

        int ID;
        Jchange [] Changes;

        public int getID() {
            return ID;
        }

        public void setID(int ID) {
            this.ID = ID;
        }

        public Jchange[] getChanges() {
            return Changes;
        }

        public void setChanges(Jchange[] changes) {
            this.Changes = changes;
        }
    }

    /**
     * Classe interne servant à la representation des données JSON
     * Cette classe représente les object_changes contenu dans le JSON
     */
    private class Jchange{
        int ID_Item;
        String Type;
        JInfo Info;

        public int getID_Item() {
            return ID_Item;
        }

        public void setID_Item(int ID_Item) {
            this.ID_Item = ID_Item;
        }

        public String getType() {
            return Type;
        }

        public void setType(String type) {
            Type = type;
        }

        public JInfo getInfo() {
            return Info;
        }

        public void setInfo(JInfo info) {
            Info = info;
        }
    }
    /**
     * Classe interne servant à la representation des données JSON
     * Cette classe représente les infos des objects_change contenu dans le JSON
     */
    private class JInfo{

        String image;
        String nom;
        String nomavant;
        String etat;
        String etatavant;
        String Type;
        String humeur;
        String humeuravant;
        String orientation;
        String orientationavant;
        boolean genre;
        boolean genreavant;
        int posX;
        int posXavant;
        int posY;
        int posYavant;
        boolean affichage;
        boolean affichageavant;

        public String getOrientation() {
            return orientation;
        }

        public void setOrientation(String orientation) {
            this.orientation = orientation;
        }

        public String getOrientationavant() {
            return orientationavant;
        }

        public void setOrientationavant(String orientationavant) {
            this.orientationavant = orientationavant;
        }

        public String getNomavant() {
            return nomavant;
        }

        public void setNomavant(String nomavant) {
            this.nomavant = nomavant;
        }

        public String getEtatavant() {
            return etatavant;
        }

        public void setEtatavant(String etatavant) {
            this.etatavant = etatavant;
        }

        public String getHumeuravant() {
            return humeuravant;
        }

        public void setHumeuravant(String humeuravant) {
            this.humeuravant = humeuravant;
        }

        public boolean isGenre() {
            return genre;
        }

        public boolean isGenreavant() {
            return genreavant;
        }

        public void setGenreavant(boolean genreavant) {
            this.genreavant = genreavant;
        }

        public int getPosXavant() {
            return posXavant;
        }

        public void setPosXavant(int posXavant) {
            this.posXavant = posXavant;
        }

        public int getPosYavant() {
            return posYavant;
        }

        public void setPosYavant(int posYavant) {
            this.posYavant = posYavant;
        }

        public void setAffichage(boolean affichage) {
            this.affichage = affichage;
        }

        public void setAffichageavant(boolean affichageavant) {
            this.affichageavant = affichageavant;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getNom() {
            return nom;
        }

        public void setNom(String nom) {
            this.nom = nom;
        }

        public String getEtat() {
            return etat;
        }

        public void setEtat(String etat) {
            this.etat = etat;
        }

        public String getType() {
            return Type;
        }

        public void setType(String type) {
            Type = type;
        }

        public String getHumeur() {
            return humeur;
        }

        public void setHumeur(String humeur) {
            this.humeur = humeur;
        }

        public boolean getGenre() {
            return genre;
        }

        public void setGenre(boolean genre) {
            this.genre = genre;
        }

        public int getPosX() {
            return posX;
        }

        public void setPosX(int posX) {
            this.posX = posX;
        }

        public int getPosY() {
            return posY;
        }

        public void setPosY(int posY) {
            this.posY = posY;
        }

        public boolean isAffichage() {
            return affichage;
        }

        public boolean isAffichageavant() {
            return affichageavant;
        }
    }

    /**
     * Fonction de loading depuis un fichier .edi
     * Il n'y a pas de vérification pour le fichier, c'est une choses à ajouter
     * @param fichier le path du fichier avec son extension
     * @return true si la sauvegarde a fonctionné, false sinon
     */
    public boolean load(String fichier ){

        try {
            Gson moteurJson = new GsonBuilder().setPrettyPrinting().create();
            //Reader reader = new InputStreamReader();
            FileReader fr = new FileReader(fichier);
            BufferedReader br = new BufferedReader(fr);

            fetchJson VALUE = moteurJson.fromJson(br, fetchJson.class);

            _piece.clean();

            for(JObjet obj : VALUE.getListObjet()){

                if(obj.type.equals("Mobilier")){
                    _idtoobjets.put(obj.id, new Mobilier(obj.image,obj.posX,obj.posY,obj.nom ,obj.etat));
                }
                if(obj.type.equals("Comedien")){
                    _idtoobjets.put(obj.id, new Comedien(obj.image, obj.posX, obj.posY, obj.nom, obj.genre, obj.humeur));
                }

                _piece.getUsedObjects().store(_idtoobjets.get(obj.id));
            }

            Controller.getInstance().getSceneView().setUsedObjects();


            for(JActe act : VALUE.actes){
                Acte a = new Acte();
                for(JScene sce : act.scenes){
                    Scene s = new Scene(Controller.getInstance().getSceneView());
                    for(Jinstant instant : sce.instants){
                        for(Jchange change : instant.Changes){

                            s.attach(instant.ID, createChange(change), true);
                        }
                    }
                    a.addScene(s);
                }
                _piece.addActe(a);
            }

        }
        catch(Exception e){
            e.printStackTrace();
            return false;
        }
        return  true;
    }

    /**
     * Fonction générale pour l'entrée des données se trouvant dans les objectschanges
     * @param item l'objectschanges a serialiser.
     * @return un objet JSON avec les donnée de ITEM
     */
    JsonObject getInfo(TimelineItem item){

        if(item instanceof ChangeOrientation){
            return getInfo((ChangeOrientation)item);
        }
        if(item instanceof ChangeHumeur){
            return getInfo((ChangeHumeur)item);
        }
        if(item instanceof ChangeNom){
            return getInfo((ChangeNom)item);
        }
        if(item instanceof ChangeGenre){
            return getInfo((ChangeGenre)item);
        }
        if(item instanceof ChangeEtat){
            return getInfo((ChangeEtat)item);
        }
        if(item instanceof ChangePos){
            return getInfo((ChangePos)item);
        }
        if(item instanceof ChangeAffichage){
            return getInfo((ChangeAffichage) item);
        }
        return new JsonObject();
    }

    /**
     * Fonction de serialisation de l'objectchange AddComedien
     * Cette fonction n'est pas utilisée
     * @param com l'ajout de comédien a serialiser
     * @return un objet JSON contenant l'object change
     */
    JsonObject getInfo(AddComedien com){
        JsonObject info = new JsonObject();

        info.add("Type", new JsonPrimitive("Comedien"));
        info.add("image", new JsonPrimitive(com.getImage()));
        info.add("nom", new JsonPrimitive(com.getNom()));
        info.add("humeur", new JsonPrimitive(com.getHumeur()));
        info.add("genre", new JsonPrimitive(com.isGenre()));
        info.add("posX", new JsonPrimitive(com.getPosX()));
        info.add("posY", new JsonPrimitive(com.getPosY()));

        return info;
    }

    /**
     * Fonction de serialisation de l'objectchange AddMobilier
     * Cette fonction n'est pas utilisée
     * @param mob l'ajout de mobilier à serialiser
     * @return un objet JSON contenant l'object change
     */
    JsonObject getInfo(AddMobilier mob){
        JsonObject info = new JsonObject();

        info.add("Type", new JsonPrimitive("Mobilier"));
        info.add("image", new JsonPrimitive(mob.getImage()));
        info.add("nom", new JsonPrimitive(mob.getNom()));
        info.add("etat", new JsonPrimitive(mob.getEtat()));
        info.add("posX", new JsonPrimitive(mob.getPosX()));
        info.add("posY", new JsonPrimitive(mob.getPosY()));

        return info;
    }

    /**
     * Fonction de serialisation de l'objectchange ChangeAffichage
     * @param aff le changement d'affichage a serialiser
     * @return un objet JSON contenant l'object change
     */
    JsonObject getInfo(ChangeAffichage aff){
        JsonObject info = new JsonObject();

        info.add("Type", new JsonPrimitive("Affichage"));
        info.add("affichage", new JsonPrimitive(aff.isAffichage()));
        info.add("affichageavant", new JsonPrimitive(aff.isAffichage_before()));

        return info;
    }
    /**
     * Fonction de serialisation de l'objectchange ChangeEtat
     * @param etat le changement d'etat a serialiser
     * @return un objet JSON contenant l'object change
     */
    JsonObject getInfo(ChangeEtat etat ){
        JsonObject info = new JsonObject();

        info.add("Type", new JsonPrimitive("Etat"));
        info.add("etat", new JsonPrimitive(etat.getEtat()));
        info.add("etatavant", new JsonPrimitive(etat.getEtat_before()));

        return info;
    }
    /**
     * Fonction de serialisation de l'objectchange ChangeGenre
     * @param genre le changement de genre a serialiser
     * @return un objet JSON contenant l'object change
     */
    JsonObject getInfo(ChangeGenre genre){
        JsonObject info = new JsonObject();

        info.add("Type", new JsonPrimitive("Genre"));
        info.add("genre", new JsonPrimitive(genre.isGenre()));
        info.add("genreavant", new JsonPrimitive(genre.isGenre_before()));

        return info;
    }
    /**
     * Fonction de serialisation de l'objectchange ChangeHumeur
     * @param humeur le changement d'humeur a serialiser
     * @return un objet JSON contenant l'object change
     */
    JsonObject getInfo(ChangeHumeur humeur){
        JsonObject info = new JsonObject();

        info.add("Type", new JsonPrimitive("Humeur"));
        info.add("humeur", new JsonPrimitive(humeur.getHumeur()));
        info.add("humeuravant", new JsonPrimitive(humeur.getHumeur_before()));

        return info;
    }
    /**
     * Fonction de serialisation de l'objectchange ChangeNom
     * @param nom le changement de nom a serialiser
     * @return un objet JSON contenant l'object change
     */
    JsonObject getInfo(ChangeNom  nom){
        JsonObject info = new JsonObject();

        info.add("Type", new JsonPrimitive("Nom"));
        info.add("nom", new JsonPrimitive(nom.getNom()));
        info.add("nomavant", new JsonPrimitive(nom.getNom_before()));

        return info;
    }
    /**
     * Fonction de serialisation de l'objectchange ChangeOrientation
     * @param ori le changement d'orientation a serialiser
     * @return un objet JSON contenant l'object change
     */
    JsonObject getInfo(ChangeOrientation ori){
        JsonObject info = new JsonObject();

        info.add("Type", new JsonPrimitive("Orientation"));
        info.add("orientation", new JsonPrimitive(ori.getNouvelle_orientation()));
        info.add("orientationavant", new JsonPrimitive(ori.getAncienne_orientation()));

        return info;
    }
    /**
     * Fonction de serialisation de l'objectchange ChangePos
     * @param pos le changement de position a serialiser
     * @return un objet JSON contenant l'object change
     */
    JsonObject getInfo(ChangePos pos){
        JsonObject info = new JsonObject();

        info.add("Type", new JsonPrimitive("Pos"));
        info.add("posX", new JsonPrimitive(pos.getPosX()));
        info.add("posY", new JsonPrimitive(pos.getPosY()));

        info.add("posXavant", new JsonPrimitive(pos.getPosX_before()));
        info.add("posYavant", new JsonPrimitive(pos.getPosY_before()));

        return info;
    }
    /**
     * Fonction de deserialisation de la partie ObjectChange de l'application
     * @param c les données pour créer l'object change
     * @return un objectChange, créer a partir des données
     */
    TimelineItem createChange(Jchange c){
        TimelineItem a = null;
        if(c.Info.Type.equals("Affichage")){
            a = new ChangeAffichage(_idtoobjets.get(c.ID_Item),c.Info.affichage, c.Info.affichageavant);
        }
        if(c.Info.Type.equals("Etat")){
            a = new ChangeEtat(_idtoobjets.get(c.ID_Item), c.Info.etat, c.Info.etatavant);
        }
        if(c.Info.Type.equals("Genre")){
            a = new ChangeGenre(_idtoobjets.get(c.ID_Item), c.Info.genre, c.Info.genreavant);
        }
        if(c.Info.Type.equals("Pos")){
            a = new ChangePos(_idtoobjets.get(c.ID_Item), c.Info.posX,c.Info.posY, c.Info.posXavant, c.Info.posYavant);
        }
        if(c.Info.Type.equals("Orientation")){
            a = new ChangeOrientation(_idtoobjets.get(c.ID_Item), c.Info.orientation, c.Info.orientationavant);
        }
        if(c.Info.Type.equals("Nom")){
            a = new ChangeNom(_idtoobjets.get(c.ID_Item), c.Info.nom,c.Info.nomavant);
        }
        if(c.Info.Type.equals("Humeur")){
            a = new ChangeHumeur(_idtoobjets.get(c.ID_Item), c.Info.humeur, c.Info.humeuravant);
        }

        return a;
    }

}
