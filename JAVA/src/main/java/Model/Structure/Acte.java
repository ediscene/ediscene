package Model.Structure;

import views.SceneView;

import java.util.ArrayList;

/**
 * Représente un acte d'une pièce donnée
 */
public class Acte {

    //Les scènes contenues dans cette pièce
    private ArrayList<Scene> scenes = new ArrayList<Scene>();

    /**
     * Créé un acte
     */
    public Acte(SceneView sceneView, UsedObjects usedObjects) {
        scenes.add(new Scene(sceneView));
    }

    /**
     * Créé un acte
     * Constructeur d?Acte pour la fonction load de la classe sauvegarde
     */
    public  Acte(){}

    /**
     * @return le nombre de scènes de cet acte
     */
    public int getNbScenes(){

        return scenes.size();
    }

    /**
     * Retournes l'indice auquel se trouve la scène donnée
     * @param sceneToFind la scène dont on souhaite connaître l'indice
     * @return l'indice
     */
    public int getIndexScene(Scene sceneToFind){

        for (int i = 0; i < scenes.size(); ++i){
            if(scenes.get(i) == sceneToFind)
                return i;
        }
        return -1;
    }

    /**
     * Ajoute une scène à cet acte
     * @param scene la scène à ajouter
     */
    public void addScene(Scene scene) {
        scenes.add(scene);
    }

    public ArrayList<Scene> getScenes() {

        return scenes;
    }

    /**
     * Retourne la scène à l'indice donné
     * @param index l'indice de la scène
     * @return la scène correspondante
     */
    public Scene getScene(int index){
        return scenes.get(index);
    }

    public void setScenes(ArrayList<Scene> scenes) {
        this.scenes = scenes;
    }
}
