package Model.Structure;

import Model.Object.Comedien;
import Model.Object.Mobilier;
import Model.Object.ObjectScene;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Le UsedObjects est un container de tous les objets concrets utilisés dans la pièce et permet
 * de les retrouver facilement et rapidement
 * Celui-ci les stocke dans une LinkedHashMap par classe d'objets
 */
public class UsedObjects {

    //Store des objets
    private LinkedHashMap<String, ArrayList<ObjectScene>> store = new LinkedHashMap<>();

    /**
     * Ajoute un ObjectScene au container
     * @param object l'objet à stocker
     */
    public void store(ObjectScene object) {

        String classname = object.getClass().getName();

        if(!store.containsKey(classname))
            store.put(classname, new ArrayList<>());

        ArrayList<ObjectScene> items = store.get(classname);
        if(!items.contains(object))
            items.add(object);
    }

    /**
     * Permet de returer tous les objets du container
     */
    public void removeAll(){
        store.clear();
    }

    /**
     * Retire tous les objets du type classname du container
     * @param classname le type d'ObjectScene à retirer
     */
    public void removeAllOfClass(String classname){
        if(store.containsKey(classname)){
            store.remove(classname);
        }
    }

    /**
     * Rempli le store à partir d'une liste d'ObjectScene
     * @param objectScenes la liste devant être copiée
     */
    public void store(List<ObjectScene> objectScenes){
        for(ObjectScene objectScene : objectScenes){
            store(objectScene);
        }
    }

    /**
     * Retournes tous les objets d'une certaine classe contenus dans le store
     * @param classname la classe d'objets à récupérer
     * @return l'ensemble des objets stockés
     */
    public List<ObjectScene> getAllOfClass(String classname) {

        return store.getOrDefault(classname, null);
    }

    /**
     * Vérifie qu'un objet soit déjà stocké dans le store
     * @param objectScene l'objet à vérifier
     * @return vrai si il s'y trouve déjà
     */
    public boolean contains(ObjectScene objectScene) {

        String classname = objectScene.getClass().getName();
        return store.containsKey(classname) && store.get(classname).contains(objectScene);
    }

    /**
     * Retournes l'ensemble des propriétés stockées dans le store
     * @return les propriétés
     */
    public ArrayList<ArrayList<String>> getAllProprieties(){

        ArrayList<ArrayList<String>> proprieties = new ArrayList<ArrayList<String>>();

        //Get all the used objects
        ArrayList<ObjectScene> objects = new ArrayList<>();
        if(store.containsKey("Model.Object.Comedien")) objects.addAll(getAllOfClass("Model.Object.Comedien"));
        if(store.containsKey("Model.Object.Mobilier")) objects.addAll(getAllOfClass("Model.Object.Mobilier"));

        for(int i = 0; i < objects.size(); ++i){

            ArrayList<String> tmp = new ArrayList<>();

            //Common info
            tmp.add(objects.get(i).getNom());
            tmp.add(objects.get(i).getImage());
            tmp.add(Integer.toString(objects.get(i).getPosX()));
            tmp.add(Integer.toString(objects.get(i).getPosY()));

            if(objects.get(i).getClass().getName().equals("Model.Object.Comedien")){
                tmp.add(String.valueOf(((Comedien) objects.get(i)).isGenre()));
                tmp.add(((Comedien) objects.get(i)).getHumeur());
            }else{
                tmp.add(((Mobilier) objects.get(i)).getEtat());
            }

            proprieties.add(tmp);

        }

        return proprieties;
    }

    /**
     * Retourne un accès direct sur les valeurs du store sous-jacent du container
     * @return une collection d'objets stockés
     */
    public Collection<ArrayList<ObjectScene>> getObjects() {

        return store.values();
    }

    /**
     * Retourne un accès direct sur les valeurs du store sous-jacent du container
     * @return une arrayList d'objets stockés
     */
    public ArrayList<ObjectScene> getArrayListObjects() {

        Collection<ArrayList<ObjectScene>> allClasses = getObjects();
        ArrayList<ObjectScene> res = new ArrayList<>();

        for(ArrayList<ObjectScene> arrayList : allClasses){
            for(ObjectScene objectScene : arrayList){
                res.add(objectScene);
            }
        }
        return res;
    }

}

