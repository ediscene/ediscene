package Model.Structure;

import Controller.Controller;
import Model.Object.ObjectScene;
import Model.Timeline.InstantT;
import Model.Timeline.TimelineItem;
import Model.Timeline.TimelineIterator;
import views.SceneView;

import java.util.*;

/**
 * Cette classe représente une scène (d'un acte) de la pièce
 */
public class Scene extends Observable implements Observer {

    private Controller ctrl = Controller.getInstance();
    private int maxIndex = 0;                                       //Index de l'instant le plus loin dans la timeline
    private Map<Integer, InstantT> items = new LinkedHashMap<>();   //Stockage des instants

    /**
     * Constructeur de scènes
     */
    public Scene(SceneView sceneView) {
        addObserver(sceneView);
        createInstantT();
    }


    /**
     * @return le nombre d'instants stockés dans la scène
     */
    public int getNbInstants(){
        return items.size();
    }

    /**
     * @return un iterateur d'instants
     */
    public TimelineIterator getIterator() {

        return new TimelineIterator(items);
    }

    /**
     * @param index l'index où se trouvera l'iterateur d'instants
     * @return un iterateur d'instants
     */
    public TimelineIterator getIteratorFrom(int index){
        TimelineIterator iterator = new TimelineIterator(items);

        while(--index > 0){
            iterator.next();
        }

        return iterator;
    }

    /**
     * Attache un TimelineItem à la scène à un instant donné
     * @param indexT l'instant
     * @param item l'objet
     */
    public void attach(Integer indexT, TimelineItem item) {
        checkOrCreateInstantT(indexT).add(item);
    }

    /**
     * Attache un TimelineItem à la scène à un instant donné depuis la fonction de load
     * @param indexT l'instant
     * @param item l'objet
     * @param dist  distinction entre les deux attach
     */
    public void attach(Integer indexT, TimelineItem item, boolean dist) {
        checkOrCreateInstantT(indexT).add(item, dist);
    }

    /**
     * Retire un TimelineItem de la scène
     * @param indexT l'instant supposé de l'item
     * @param item l'item
     */
    public void remove(Integer indexT, TimelineItem item) {

        if(contains(indexT)) {

            InstantT instantT = items.get(indexT);
            instantT.remove(item);

            if(instantT.getItems().isEmpty())
                items.remove(indexT);
        }
    }

    /**
     * Vérifie que la scène ait des éléments à l'instant donné
     * @param indexT l'instant à vérifier
     * @return true si des objets se trouvent à l'instant donné
     */
    public boolean contains(Integer indexT) {

        return items.containsKey(indexT);
    }

    /**
     * Retourne la liste d'éléments de l'instant T à l'index donné
     * Attention cette méthode crée un instantT si il n'en existe pas!
     * @param indexT l'instant donné
     * @return une liste de TimelineItems
     */
    public List<TimelineItem> getItems(Integer indexT) {

        return checkOrCreateInstantT(indexT).getItems();
    }



    /**
     * Retourne l'ensemble des TimelineItem (ObjectChange) liés à un ObjectScene entre deux moments donnés
     * @param from  Instant de début
     * @param to    Instant de fin
     * @return      une belle LinkedHashMap
     */
    public Map<ObjectScene, ArrayList<TimelineItem>> computeChanges(int from, int to) {

        LinkedHashMap<ObjectScene, ArrayList<TimelineItem>> results = new LinkedHashMap<>();

        for(int i = from; i < to + 1; i++) {

            if(contains(i)) {

                InstantT instantT = items.get(i);

                for(TimelineItem item : instantT.getItems()) {

                    if(!results.containsKey(item.getItem()))
                        results.put(item.getItem(), new ArrayList<TimelineItem>());

                    results.get(item.getItem()).add(item);
                }
            }
        }

        return results;
    }

    /**
     * Retourne tous les ObjectChange liés à un ObjectScene donné entre deux moments donnés
     * @param item  L'objet dont on veut les objectChange
     * @param from  Instant de début
     * @param to    Instant de fin
     * @return      Une belle liste d'ObjectChange
     */
    public List<TimelineItem> computeChanges(ObjectScene item, int from, int to) {
        return computeChanges(from, to).get(item);
    }

    /**
     * Applique tous les changeObject à item à un instant donné
     * @param item  L'objet dont on veut les objectChange
     * @param instantIndex L'index de l'instant dont on veut les ObjectChanges
     */
    public void applyChanges(ObjectScene item, int instantIndex) {
        List<TimelineItem> changes = computeChanges(item, instantIndex, instantIndex);//.get(item);

        if(changes == null) return;

        for(TimelineItem change : changes){
            change.change();
        }
    }

    /**
     * Enlève tous les changeObject à item à un instant donné
     * @param item  L'objet dont on veut enlever les objectChange
     * @param instantIndex L'index de l'instant dont on veut enlever les ObjectChanges
     */
    public void removeChanges(ObjectScene item, int instantIndex) {
        List<TimelineItem> changes = computeChanges(item, instantIndex, instantIndex);//.get(item);

        if(changes == null) return;

        // doit etre fait dans l'autre sens
        for(TimelineItem change : changes){
           change.undo();
        }
    }

    public int getIndexPresentInstant(){
        int index = 0;
        while(items.containsKey(index))
            ++index;
        return index;
    }

    @Override
    public void update(Observable o, Object arg) {
        setChanged();
        notifyObservers(arg);
    }

    /**
     * @return l'instant courant sur lequel la scène pointe
     */
    public InstantT getCurrentInstant() {
        return checkOrCreateInstantT(ctrl.getCurrentInstant());
    }

    /**
     * @return l'indice courant sur lequel la scène pointe
     */
    public int getCurrentIndex() {
        return ctrl.getCurrentInstant();
    }

    /**
     * Retourne au début
     */
    public void goToBeginning() {
        ctrl.setCurrentInstant(0);
        //notifyObservers();
    }

    /**
     * Avance la scène d'un certain temps
     * @param i le temps avancé
     */
    public void forward(int i) {
        ctrl.setCurrentInstant(ctrl.getCurrentInstant() + i);
        //notifyObservers();
    }

    /**
     * Rembobinne la scène d'un certain temps
     * @param i le temps à reculer
     */
    public void backward(int i) {

        if(ctrl.getCurrentInstant() - i < 0)
            throw new RuntimeException("This is even worse than the matrix!");

        ctrl.setCurrentInstant(ctrl.getCurrentInstant() - i);
        //notifyObservers();
    }

    /**
     * Vérifie qu'un instantT existe à un indice donné. S'il existe, le retourne, sinon en
     * crée un, l'ajoute à la Scène et le retourne
     * @param i L'indice temporel à vérifier
     * @return  un InstantT
     */
    private InstantT checkOrCreateInstantT(int i) {

        if(!contains(i))
            items.put(i, new InstantT(i));

        if(maxIndex < i)
            maxIndex = i;

        return items.get(i);
    }

    /**
     * Ajoute un nouvel instant
     * @return  un InstantT
     */
    public InstantT createInstantT() {

        int count = getNbInstants();
        while(contains(count)){
            count++;
        }

        InstantT instantT = new InstantT(count);
        items.put(count, instantT);

        return instantT;
    }

}

