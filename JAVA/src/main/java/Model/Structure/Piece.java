package Model.Structure;

import views.SceneView;

import java.util.ArrayList;

/**
 * Représente une pièce de théatre. Cette pièce est composée d'actes, eux-même
 * composés de scènes, elles-même composées d'instantT. On garde également une liste
 * des ObjectScene qui ont été utilisés durant la pièce.
 */
public class Piece {

    private ArrayList<Acte> actes   = new ArrayList<Acte>();        //Les différents actes qui composent la pièce
    private UsedObjects usedObjects = new UsedObjects();         //La liste des ObjectScene utilisés durant la pièce

    /**
     * Créé une pièce
     */
    public Piece(SceneView sceneView) {
        actes.add(new Acte(sceneView, usedObjects));
    }


    /**
     * Charge une pièce
     * @param piece la pièce à charger
     */
    public Piece(Piece piece) {
        this.actes = piece.getActes();
    }

    /**
     * Ajoute un acte à cette pièce
     * @param acte
     */
    public void addActe(Acte acte) {

        actes.add(acte);
    }

    /**
     * @return le nombre d'actes de la pièce
     */
    public int getNbActes(){
        return actes.size();
    }

    /**
     * Retourne l'indice de l'acte donné
     * @param acteToFind l'acte dont on veut l'indice
     * @return l'indice de l'acte
     */
    public int getIndexActe(Acte acteToFind){
        for (int i = 0; i < actes.size(); ++i){
            if(actes.get(i) == acteToFind)
                return i;
        }
        return -1;
    }

    /**
     * Assigne un nouveau tableau d'actes
     */
    public void setActes(ArrayList<Acte> actes) {
        this.actes = actes;
    }

    /**
     * @return les différents actes de la pièce
     */
    public ArrayList<Acte> getActes() {

        return actes;
    }

    /**
     * Retourne l'acte pointé par l'indice donné
     * @param index l'indice de l'acte
     * @return  l'acte correspondant
     */
    public Acte getActe(int index){
        return actes.get(index);
    }

    public UsedObjects getUsedObjects() {
        return usedObjects;
    }

    /**
     * Permet de nettoyer la pièce
     */
    public void clean(){
        actes.clear();
        usedObjects.removeAll();
    }
}
