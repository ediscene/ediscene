package Model.Timeline;

import Model.Object.ObjectScene;
import Model.Structure.Scene;
import Model.Structure.UsedObjects;
import org.junit.Test;
import views.SceneView;

import static org.junit.Assert.assertEquals;

public class TimelineIteratorTest {

    @Test
    public void sceneShouldAttachItems() {

        UsedObjects usedObjects = new UsedObjects();
        Scene scene = new Scene(new SceneView());

        /*scene.attach(0, new TimelineItem() {
            public ObjectScene getItem() {
                return null;
            }
        });

        scene.attach(1, new TimelineItem() {
            public ObjectScene getItem() {
                return null;
            }
        });

        scene.attach(5, new TimelineItem() {
            public ObjectScene getItem() {
                return null;
            }
        });

        scene.attach(6, new TimelineItem() {
            public ObjectScene getItem() {
                return null;
            }
        });*/

        TimelineIterator iterator = scene.getIterator();

        int i = 0;
        while(iterator.hasNext() && iterator.next() != null)
            i++;

        assertEquals(4, i);
    }
}
