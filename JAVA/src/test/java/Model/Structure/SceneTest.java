package Model.Structure;

import Model.Object.Comedien;
import Model.Object.ObjectScene;
import Model.Timeline.TimelineItem;
import org.junit.Test;
import views.SceneView;

import static org.junit.Assert.*;

/**
 * @author Guillaume Hochet
 */
public class SceneTest {

    @Test
    public void sceneShouldAttachItems() {

        UsedObjects usedObjects = new UsedObjects();
        SceneView sceneView = new SceneView();
        Scene scene = new Scene(sceneView);
        ObjectScene link = new Comedien("src/main/resources/imageObjectScene/perso1.png", 2, 3, "dupont", true, "");

        TimelineItem t1 = null;//() ->  link;
        TimelineItem t2 = null;//() ->  link;

        scene.attach(1, t1);
        scene.attach(2, t2);

        assertTrue(scene.contains(1));
        assertTrue(scene.contains(2));
    }

    @Test
    public void sceneShouldRemoveItems() {

        UsedObjects usedObjects = new UsedObjects();
        SceneView sceneView = new SceneView();
        Scene scene = new Scene(sceneView);
        ObjectScene link = new Comedien("src/main/resources/imageObjectScene/perso1.png", 2, 3, "dupont", true, "");

        TimelineItem t1 = null;//() -> link;

        scene.attach(1, t1);

        assertTrue(scene.contains(1));
        assertTrue(scene.getItems(1).contains(t1));

        scene.remove(1, t1);

        assertFalse(scene.contains(1));
    }
}
