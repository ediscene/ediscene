package Model.Structure;

import Model.Object.Comedien;
import Model.Object.ObjectScene;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import static org.junit.Assert.*;

/**
 * @author Guillaume Hochet
 */
public class UsedObjectsTest {

    @Test
    public void usedObjectsShouldStoreSameItemOnlyOnce() {

        UsedObjects uso = new UsedObjects();
        ObjectScene o1 = new Comedien("swag", 1, 1, "o1", true, "");
        ObjectScene o2 = new Comedien("yolo", 1, 1, "o2", true, "");

        assertNull(uso.getAllOfClass(o1.getClass().getName()));
        uso.store(o1);
        assertEquals(uso.getAllOfClass(o1.getClass().getName()).size(), 1);

        uso.store(o1);
        uso.store(o2);

        assertEquals(uso.getAllOfClass(o1.getClass().getName()).size(), 2);
    }

    @Test
    public void usedObjectsShouldContainAddedItems() {

        UsedObjects uso = new UsedObjects();
        ObjectScene o1 = new Comedien("swag", 1, 1, "o1", true, "");
        ObjectScene o2 = new Comedien("yolo", 1, 1, "o2", true, "");

        uso.store(o1);

        assertTrue(uso.contains(o1));
        assertFalse(uso.contains(o2));

        uso.store(o2);
        assertTrue(uso.contains(o2));
    }

    @Test
    public void usedObjectsShouldReturnAllItemsStored() {

        UsedObjects uso = new UsedObjects();
        ObjectScene o1 = new Comedien("swag", 1, 1, "o1", true, "");
        ObjectScene o2 = new Comedien("yolo", 1, 1, "o2", true, "");

        uso.store(o1);
        uso.store(o2);

        Collection<ArrayList<ObjectScene>> items = uso.getObjects();
        Iterator<ArrayList<ObjectScene>> iterator = items.iterator();

        while(iterator.hasNext()) {

            ArrayList<ObjectScene> list = iterator.next();
            assertTrue(list.contains(o1));
            assertTrue(list.contains(o2));
        }
    }

}
